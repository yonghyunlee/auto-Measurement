/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  MEAS                             1       /* callback function: measPanel */
#define  MEAS_TAB                         2       /* control type: tab, callback function: MAINTAB */

#define  SETUPPANEL                       2       /* callback function: setupPanel */
#define  SETUPPANEL_SETUPBTN              2       /* control type: command, callback function: setupBtnCallback */
#define  SETUPPANEL_GERNERATOR            3       /* control type: ring, callback function: siggenCallback */
#define  SETUPPANEL_ANALYZER              4       /* control type: ring, callback function: specAnalCallback */
#define  SETUPPANEL_SELETEDSIGTEXT        5       /* control type: textMsg, callback function: (none) */
#define  SETUPPANEL_SELECTEDANAL          6       /* control type: textMsg, callback function: (none) */

     /* tab page panel controls */
#define  TABAMP_SAVE                      2       /* control type: command, callback function: AmpSaveBT */
#define  TABAMP_START_BT                  3       /* control type: command, callback function: AmpStartBT */
#define  TABAMP_P_OUT                     4       /* control type: numeric, callback function: (none) */
#define  TABAMP_CANCEL                    5       /* control type: command, callback function: AmpCancelBT */
#define  TABAMP_SAVE_FILENAME             6       /* control type: string, callback function: (none) */
#define  TABAMP_SPLITTER_2                7       /* control type: splitter, callback function: (none) */
#define  TABAMP_FREQ                      8       /* control type: scale, callback function: (none) */
#define  TABAMP_GRAPH                     9       /* control type: graph, callback function: (none) */
#define  TABAMP_P_IN                      10      /* control type: numeric, callback function: (none) */
#define  TABAMP_GAIN                      11      /* control type: numeric, callback function: (none) */
#define  TABAMP_SETTITLE                  12      /* control type: textMsg, callback function: (none) */
#define  TABAMP_STEP                      13      /* control type: numeric, callback function: (none) */
#define  TABAMP_STARTPOW                  14      /* control type: scale, callback function: (none) */
#define  TABAMP_STOPPOW                   15      /* control type: scale, callback function: (none) */

     /* tab page panel controls */
#define  TABCON_MEAS_SAVE                 2       /* control type: command, callback function: saveCallback */
#define  TABCON_MEAS_START_BT             3       /* control type: command, callback function: measBTCallback */
#define  TABCON_P1dB_OUT                  4       /* control type: numeric, callback function: (none) */
#define  TABCON_FLAT_LEAK                 5       /* control type: numeric, callback function: (none) */
#define  TABCON_P_OUT                     6       /* control type: numeric, callback function: (none) */
#define  TABCON_CANCEL                    7       /* control type: command, callback function: cancelCallback */
#define  TABCON_SAVE_FILENAME             8       /* control type: string, callback function: (none) */
#define  TABCON_SPLITTER_2                9       /* control type: splitter, callback function: (none) */
#define  TABCON_SPLITTER                  10      /* control type: splitter, callback function: (none) */
#define  TABCON_FREQ                      11      /* control type: scale, callback function: (none) */
#define  TABCON_SETTITLE_2                12      /* control type: textMsg, callback function: (none) */
#define  TABCON_STARTPOW                  13      /* control type: scale, callback function: (none) */
#define  TABCON_STOPPOW                   14      /* control type: scale, callback function: (none) */
#define  TABCON_GRAPH                     15      /* control type: graph, callback function: (none) */
#define  TABCON_P_IN                      16      /* control type: numeric, callback function: (none) */
#define  TABCON_GAIN                      17      /* control type: numeric, callback function: (none) */
#define  TABCON_SETTITLE                  18      /* control type: textMsg, callback function: (none) */
#define  TABCON_AMPLIST                   19      /* control type: listBox, callback function: ampListCallback */
#define  TABCON_SELECTED_AMP              20      /* control type: string, callback function: (none) */
#define  TABCON_STEP                      21      /* control type: numeric, callback function: (none) */
#define  TABCON_TEXTMSG                   22      /* control type: textMsg, callback function: (none) */
#define  TABCON_TEXTMSG_2                 23      /* control type: textMsg, callback function: (none) */

     /* tab page panel controls */
#define  TABREF_SAVE                      2       /* control type: command, callback function: refSaveBT */
#define  TABREF_STARTPOW                  3       /* control type: scale, callback function: powerSet */
#define  TABREF_START_BT                  4       /* control type: command, callback function: refStartCallback */
#define  TABREF_APPLY                     5       /* control type: command, callback function: refApplyCallback */
#define  TABREF_SETTITLE                  6       /* control type: textMsg, callback function: (none) */
#define  TABREF_SPLITTER_2                7       /* control type: splitter, callback function: (none) */
#define  TABREF_SAVE_FILENAME             8       /* control type: string, callback function: (none) */
#define  TABREF_OUTPUT                    9       /* control type: numeric, callback function: output */
#define  TABREF_REFLVLOFFSET              10      /* control type: numeric, callback function: (none) */
#define  TABREF_FREQ                      11      /* control type: scale, callback function: (none) */


     /* Control Arrays: */

          /* (no control arrays in the resource file) */


     /* Menu Bars, Menus, and Menu Items: */

          /* (no menu bars in the resource file) */


     /* Callback Prototypes: */

int  CVICALLBACK AmpCancelBT(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ampListCallback(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK AmpSaveBT(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK AmpStartBT(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cancelCallback(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK MAINTAB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK measBTCallback(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK measPanel(int panel, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK output(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK powerSet(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK refApplyCallback(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK refSaveBT(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK refStartCallback(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK saveCallback(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK setupBtnCallback(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK setupPanel(int panel, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK siggenCallback(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK specAnalCallback(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif
