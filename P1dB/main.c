#include <formatio.h>
#include <utility.h>
#include <visa.h>
#include <ansi_c.h>
#include <cvirte.h>		
#include <userint.h>
#include "P1dB_UI.h"

static int setuppanel;
static int measpanel;
static int ConTabPanelHandle;
static int RefTabPanelHandle;
static int AmpTabPanelHandle;

static int powerSetting (double value);

static char instrDescriptor[VI_FIND_BUFLEN]={0,};  
static ViUInt32 numInstrs;  
static ViFindList findList;  
static ViSession defaultRM, instr, instrSpecAna, instrSigGen;  
static ViStatus status;  

static ViUInt32 retCount;
static ViUInt32 writeCount;
static unsigned char buffer[100];
static char stringinput[512], textBox[512]="";
static char descriptionSpecAna[20]="GPIB::", descriptionSigGen[20]="GPIB::",strTemp[20]="";

static int panelHandle, iTest, divValue,numPoints;

static double reflvloffset;

static double pwrStart, pwrStep, pwrStop, p1dBIn, p1dBOut, p1dBx;     
static double measFreq, measGain, measAtten, measPower;     
static double x[100],y[100], gain[100], pwrHigh, pwrLow,waitingTime=1;
static double flat_leak;

static double ampStart, ampStop, ampStep, ampFreq, amp_p1dBIn, amp_p1dBOut, amp_x[100], amp_y[100], amp_gain[100]= {0, };
double ampGainT=0, ampSlope = 0;
static int amp_num=0;

static int threadFunctionID, AmpThreadFunctionID;
static int CVICALLBACK measThread (void *functionData);
static int CVICALLBACK ampThread (void *functionData);
static int cancel = 0;

static int ampCheck = 0;
static int onePointCheck = 0;

static char	*z = NULL;
static struct tm	*tm;
static time_t	tt;

static char currentPath[MAX_PATHNAME_LEN];

int main (int argc, char *argv[])
{
	if (InitCVIRTE (0, argv, 0) == 0)
		return -1;	/* out of memory */
	if ((setuppanel = LoadPanel (0, "P1dB_UI.uir", SETUPPANEL)) < 0)
		return -1;
	/* 1.0 Resource Manager session open (defaultRM)*/  
	
   status = viOpenDefaultRM(&defaultRM);  
   if (status != VI_SUCCESS)  
   {  
      printf("Could not open a session to the VISA Resource Manager!\n");  
      exit (EXIT_FAILURE);  
   }   

/* 1.1 equipment list & 1st ******** intrumentDescriptor */  
   
   status = viFindRsrc(defaultRM, "?*INSTR", &findList, &numInstrs, instrDescriptor);  

   if (status < VI_SUCCESS)  
   {  
	   goto ErrInFindInstruments;
   }  
  
   				printf("%d instruments, serial ports, and other resources found:\n\n",numInstrs);  ////
   				printf("VISA Resource : %s \n",instrDescriptor);  
   strcat(textBox,"VISA Resource : ");															   ////
   strcat(textBox,instrDescriptor);
   strcat(textBox,"\n");

/* 1.2 1st ******* instrument session open (instr) */  
   
   status = viOpen (defaultRM, instrDescriptor, VI_NULL, VI_NULL, &instr);
   if (status < VI_SUCCESS)  
   {
        printf ("Cannot open a session to the device.\n");
        goto ErrInOpenInstrument;
   }


/* 1.3  write to the 1st instrument session (instr) */  

   strcpy(stringinput,"*IDN?");
   status = viWrite (instr, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
   if (status < VI_SUCCESS)  
   {
      printf("Error writing to the device\n");
      goto ErrInReadWrite;
   }

/* 1.4  read from the 1st instrument session (instr) */  
   
   status = viRead (instr, buffer, 100, &retCount);
   if (status < VI_SUCCESS) 
   {
      printf("Error reading a response from the device\n");
      goto ErrInReadWrite;   
   }
   
								      printf("Model : %*s\n",retCount,buffer);  ////
   strcat(textBox,"Model : ");
   strcat(textBox,buffer);
   strcat(textBox,"\n");

   status=viClose(instr);

   InsertListItem (setuppanel, SETUPPANEL_GERNERATOR, 0, buffer, instrDescriptor);
   InsertListItem (setuppanel, SETUPPANEL_ANALYZER, 0, buffer, instrDescriptor);
          
   while (--numInstrs)  

   {  

	   
/* 2.1 find next ******** intrumentDescriptor */  
	   
      status = viFindNext (findList, instrDescriptor);  /* find next desriptor */  
      if (status < VI_SUCCESS)   
      {     
         printf ("An error occurred finding the next resource.\nHit enter to continue.");  
         goto ErrInFindInstruments;   
      }   
								      printf("VISA Resource : %s \n",instrDescriptor);  ////

	  strcat(textBox,"VISA Resource : ");
      strcat(textBox,instrDescriptor);
      strcat(textBox,"\n");

/* 2.2 Instrument session open (instr) */  

	  status = viOpen (defaultRM, instrDescriptor, VI_NULL, VI_NULL, &instr);  
      if (status < VI_SUCCESS)  
      {  
          printf ("An error occurred opening a session to %s\n",instrDescriptor); 
		  goto ErrInOpenInstrument;
      }  
        
/* 2.3  write to the next instrument session (instr) */  

   	  strcpy(stringinput,"*IDN?");
      status = viWrite (instr, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
      if (status < VI_SUCCESS)  
      {
         printf("Error writing to the device\n");
         goto ErrInReadWrite;
      }

/* 2.4  read from the next instrument session (instr) */  
   
      status = viRead (instr, buffer, 100, &retCount);
      if (status < VI_SUCCESS) 
      {
         printf("Error reading a response from the device\n");
         goto ErrInReadWrite;   
      }
  
	   strcat(textBox,"Model : ");
	   strcat(textBox,buffer);
	   strcat(textBox,"\n");
      										printf("Model : %*s\n",retCount,buffer);	  ////

      status=viClose(instr);
	  
	  InsertListItem (setuppanel, SETUPPANEL_GERNERATOR, 0, buffer, instrDescriptor);
	  InsertListItem (setuppanel, SETUPPANEL_ANALYZER, 0, buffer, instrDescriptor);

   }    /* end while */  
   
	DisplayPanel(setuppanel);
	//  SetCtrlVal(SETUP,SETUP_TEXTBOX,"Test");
	//  SetCtrlVal(SETUP,SETUP_ADDSIGGEN,10);
	//  GetCtrlVal(panelHandle,SETUP_ADDSIGGEN,&iTest);
	//       printf("sigGen Add : %d\n",iTest);	 ////

	RunUserInterface();
// DiscardPanel(panelHandle);

   
   
   
			  
   
   
   
   
   
   
   
   

	goto ErrInOpenInstrument;

	ErrInReadWrite:
	   status = viClose(instr);
   
	ErrInOpenInstrument: 
	   status = viClose(findList); 
   
	ErrInFindInstruments:
	   status = viClose(defaultRM);
   
//   printf(textBox);
   
   printf("Closing Sessions\nHit enter to continue.");
   fflush(stdin);
//   getchar();  
  
   return status;
}

int powerSetting (double value) {
	if(value > 15) return 0;
	sprintf(strTemp,"%f", value);
	strcpy(stringinput,":POW ");
   	strcat(stringinput,strTemp);
   	strcat(stringinput,"dBm");
	return 1;
}

int CVICALLBACK measPanel (int panel, int event, void *callbackData,
						   int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_GOT_FOCUS:
			
			break;
		case EVENT_LOST_FOCUS:

			break;
		case EVENT_CLOSE:
			QuitUserInterface(0);

			break;
	}
	return 0;
}

int CVICALLBACK setupPanel (int panel, int event, void *callbackData,
							int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_GOT_FOCUS:

			break;
		case EVENT_LOST_FOCUS:

			break;
		case EVENT_CLOSE:
			QuitUserInterface(0);
			break;
	}
	return 0;
}

int CVICALLBACK measBTCallback (int panel, int control, int event,
								void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:
			SetCtrlAttribute (panel, TABCON_CANCEL, ATTR_DIMMED, 0);
			SetCtrlAttribute (panel, control, ATTR_DIMMED, 1);
			CmtScheduleThreadPoolFunction (DEFAULT_THREAD_POOL_HANDLE, measThread, NULL, &threadFunctionID);//schedule the thread passing our functions
			
			CmtWaitForThreadPoolFunctionCompletion(DEFAULT_THREAD_POOL_HANDLE, threadFunctionID, OPT_TP_PROCESS_EVENTS_WHILE_WAITING); //wait for the thread to complete
			CmtReleaseThreadPoolFunctionID(DEFAULT_THREAD_POOL_HANDLE, threadFunctionID); //release thread resources.
			
			 /* Signal Generator Power ON */
			strcpy(stringinput,":OUTP OFF");
			            								printf("\"%s\"\n",stringinput);	 ////     
			status = viWrite (instrSigGen, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
			if (status < VI_SUCCESS)  
			{
			   printf("Error writing to the device\n");
			   return -1;
			}
			SetCtrlAttribute (panel, TABCON_CANCEL, ATTR_DIMMED, 1);
			SetCtrlAttribute (panel, control, ATTR_DIMMED, 0);
			cancel = 0;
			break;
	}
	return 0;
}

int CVICALLBACK cancelCallback (int panel, int control, int event,
								void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:
			if (cancel == 0) {
				cancel = 1;
			}
			SetCtrlAttribute (panel, control, ATTR_DIMMED, 1);
			
			break;
	}
	return 0;
}

int CVICALLBACK autoCallback (int panel, int control, int event,
							  void *callbackData, int eventData1, int eventData2)
{
	int isCheck = 0;
	switch (event)
	{
		case EVENT_COMMIT:
			GetCtrlVal(panel, control, &isCheck);
			
			if(isCheck == 1) {
				onePointCheck = 1;
				SetCtrlAttribute (panel, TABCON_STOPPOW, ATTR_DIMMED, 1);
				SetCtrlAttribute (panel, TABCON_STEP, ATTR_DIMMED, 1);
				SetCtrlAttribute (panel, TABCON_STARTPOW, ATTR_LABEL_TEXT, "one power (dBm)");
			} else {
				onePointCheck = 0;
				SetCtrlAttribute (panel, TABCON_STOPPOW, ATTR_DIMMED, 0);
				SetCtrlAttribute (panel, TABCON_STEP, ATTR_DIMMED, 0);
				SetCtrlAttribute (panel, TABCON_STARTPOW, ATTR_LABEL_TEXT, "start power (dBm)");
			}
			break;
	}
	return 0;
}

int CVICALLBACK setupBtnCallback (int panel, int control, int event,
								  void *callbackData, int eventData1, int eventData2)
{
	char ampListPath[MAX_PATHNAME_LEN];
	char ampFile[MAX_PATHNAME_LEN];
	int r;
	switch (event)
	{
		case EVENT_COMMIT:		  
			
			GetCtrlVal(panel,SETUPPANEL_SELECTEDANAL,descriptionSpecAna);		///////////////////////????????????????????????????????
			GetCtrlVal(panel,SETUPPANEL_SELETEDSIGTEXT,descriptionSigGen);			///////////////////////???????????????????????????????? panelHandle -> SETUP
			
			printf("Signal Generator : %s\n", descriptionSigGen);
			printf("Signal Analyzer : %s\n\n", descriptionSpecAna);
			DiscardPanel(panel);
			
			if ((panel=LoadPanel(0,"P1dB_UI.uir", MEAS))<0)
				return -1;
			DisplayPanel(panel);
			measpanel = panel;
			
			GetPanelHandleFromTabPage (measpanel, MEAS_TAB, 0, &ConTabPanelHandle);
			GetPanelHandleFromTabPage (measpanel, MEAS_TAB, 2, &RefTabPanelHandle);
			GetPanelHandleFromTabPage (measpanel, MEAS_TAB, 1, &AmpTabPanelHandle);
			
			GetDir(currentPath);
			MakePathname (currentPath, "amp\\*", ampListPath);
			r = GetFirstFile (ampListPath, 1, 0, 0, 0, 0, 0, ampFile);
			InsertListItem (ConTabPanelHandle, TABCON_AMPLIST, -1, ampFile, ampFile);
			if (!r) {
				while(!r) {
					r = GetNextFile (ampFile);
					InsertListItem (ConTabPanelHandle, TABCON_AMPLIST, -1, ampFile, ampFile);
				};
			}
			
			break;
	}
	return 0;
}

int CVICALLBACK siggenCallback (int panel, int control, int event,
								void *callbackData, int eventData1, int eventData2)
{
	char instr[VI_FIND_BUFLEN];
	switch (event)
	{
		case EVENT_COMMIT:
			GetCtrlVal (panel, control, instr);
			SetCtrlVal(panel, SETUPPANEL_SELETEDSIGTEXT, instr);
			break;
	}
	return 0;
}

int CVICALLBACK specAnalCallback (int panel, int control, int event,
								  void *callbackData, int eventData1, int eventData2)
{
	char instr[VI_FIND_BUFLEN];
	switch (event)
	{
		case EVENT_COMMIT:
			GetCtrlVal (panel, control, instr);
			SetCtrlVal(panel, SETUPPANEL_SELECTEDANAL, instr);
			break;
	}
	return 0;
}

int CVICALLBACK saveCallback (int panel, int control, int event,
							  void *callbackData, int eventData1, int eventData2)
{
	FILE* saveFile;
	char fileName[MAX_PATHNAME_LEN];
	char filePath[MAX_PATHNAME_LEN];
	char msg[100];
	switch (event)
	{
		case EVENT_COMMIT:
			
			time (&tt);
			tm = localtime (&tt);
			z = asctime (tm);
			
			DebugPrintf ("System time: %s\n", z);
			
			strcpy(filePath,"p1dB\\");
			GetCtrlVal(panel, TABCON_SAVE_FILENAME, fileName);
			strcat(filePath, fileName);
			saveFile = OpenFile (filePath, VAL_WRITE_ONLY, VAL_OPEN_AS_IS, VAL_ASCII);
			sprintf (msg, "#logFile\n");
			WriteLine (saveFile, msg, -1);				   
			sprintf (msg, "#measure time: %s\n", z);
			WriteLine (saveFile, msg, -1);
			sprintf (msg, "@power_start:%f\n", pwrStart);
			WriteLine (saveFile, msg, -1);
			sprintf (msg, "@power_stop:%f\n", pwrStop);
			WriteLine (saveFile, msg, -1);
			sprintf (msg, "@power_step:%f\n", pwrStep);
			WriteLine (saveFile, msg, -1);
			sprintf (msg, "@frequecy:%f\n", measFreq);
			WriteLine (saveFile, msg, -1);
			
			int i=0;
			for(i=0; i<numPoints; i++){
				sprintf (msg, "p.in:%f	p.out:%f	gain:%f\n", x[i], y[i], gain[i]);
				WriteLine (saveFile, msg, -1);
			}
			
			sprintf (msg, "P1dB_IN:%f	P1dB_OUT:%f\n", p1dBIn, p1dBx);
			WriteLine (saveFile, msg, -1);
			
			sprintf (msg, "flat leakage:%f\n", flat_leak);
			WriteLine (saveFile, msg, -1);
			CloseFile (saveFile);
			
			MessagePopup("success", "success Write file");
			
			break;
	}
	return 0;
}

int CVICALLBACK ampListCallback (int panel, int control, int event,
								 void *callbackData, int eventData1, int eventData2)
{
	FILE* ampFile;
	char fileName[MAX_PATHNAME_LEN];
	char filePath[MAX_PATHNAME_LEN];
	int error;
	char buf[100];
	char *pch;
	static double* tokens;
	
	switch (event)
	{
		case EVENT_COMMIT:
			amp_num=0;
			GetCtrlVal(panel, control, fileName);
			strcpy(filePath,"amp\\");
   			strcat(filePath, fileName);
			ampFile = OpenFile (filePath, VAL_READ_ONLY, VAL_OPEN_AS_IS, VAL_ASCII);
			
			while ((error = ReadLine (ampFile, buf, -1)) != -2) {
			    if (error == -1) {
			        error = GetFmtIOError ();
					MessagePopup("Error", GetFmtIOErrorString (error));
			    }
				
				if(buf[0] == '#') continue;
				else if(buf[0] == '@') {
					char *key, *val;
					key=strtok(buf,":");
					val=strtok(NULL,":");
														  
					if(strcmp(key, "@power_start")==0) {
						printf("amp_power_start: %s\n", val);
						ampStart = atof(val);
						SetCtrlVal (ConTabPanelHandle, TABCON_STARTPOW, ampStart);
					}
					if(strcmp(key, "@power_stop")==0) {
						printf("amp_power_stop: %s\n", val);
						ampStop = atof(val);
						SetCtrlVal (ConTabPanelHandle, TABCON_STOPPOW, ampStop);
					}
					if(strcmp(key, "@power_step")==0) {
						printf("amp_power_step: %s\n", val);
						ampStep = atof(val);
						SetCtrlVal (ConTabPanelHandle, TABCON_STEP, ampStep);
					}
					if(strcmp(key, "@frequecy")==0) {
						printf("frequecy: %s\n", val);
						ampFreq = atof(val);
						SetCtrlVal (ConTabPanelHandle, TABCON_FREQ, ampFreq);
					}
				} else if (buf[0] == '*') {
					char *key, *val;
					key=strtok(buf,":");
					val=strtok(NULL,":");

					if(strcmp(key, "*reflvloffset")==0) {
						printf("reflvloffset: %s\n", val);
						amp_p1dBIn = atof(val);
					}
					if(strcmp(key, "*P1dB_OUT")==0) {
						printf("P1dB_OUT: %s\n", val);
						amp_p1dBOut = atof(val);
					}
				} else if (buf[0] == '^'){
					char *xc, *yc, *gainc, *val;
					//tokens = (double *)malloc(3 * sizeof(double));
					
					//printf("nothing: %s\n", buf);
					
					xc=strtok(buf, ",");
					yc=strtok(NULL,",");
					gainc=strtok(NULL, ",");

					strtok(xc, ":");
					val = strtok(NULL, ":");
					amp_x[amp_num] = atof(val);
					strtok(yc, ":");
					val = strtok(NULL, ":");
					amp_y[amp_num] = atof(val);
					strtok(gainc, ":");
					val = strtok(NULL, ":");
					amp_gain[amp_num] = atof(val);
					amp_num++;
				}
			}
			
			printf("%d count\n", amp_num);
			int i=0;
			for(i=0; i<amp_num; i++){
				printf("%f, %f, %f\n", amp_x[i], amp_y[i], amp_gain[i]);
			}
			SetCtrlVal(panel, TABCON_SELECTED_AMP, fileName);
			break;
	}
	return 0;
}

static int CVICALLBACK measThread (void *functionData){
	int i =0;
	cancel = 0;
	double tmpx, tmpy, tmpGain;
	
	GetCtrlVal(ConTabPanelHandle, TABCON_FREQ,&measFreq);
	GetCtrlVal(ConTabPanelHandle,TABCON_STARTPOW,&pwrStart);
	GetCtrlVal(ConTabPanelHandle,TABCON_STEP,&pwrStep);
	GetCtrlVal(ConTabPanelHandle,TABCON_STOPPOW,&pwrStop);

	SetCtrlVal (ConTabPanelHandle, TABCON_GAIN, -999.0);
	//SetCtrlVal (ConTabPanelHandle, TABCON_P1dB_IN, 999.0);
	SetCtrlVal (ConTabPanelHandle, TABCON_P1dB_OUT, 999.0);
	
	status = viOpen (defaultRM, descriptionSpecAna, VI_NULL, VI_NULL, &instrSpecAna);
   if (status < VI_SUCCESS)  
   {
      printf ("Cannot open a session to the device.\n");
      return -1;
   }
   status = viOpen (defaultRM, descriptionSigGen, VI_NULL, VI_NULL, &instrSigGen);
   if (status < VI_SUCCESS)  
   {
      printf ("Cannot open a session to the device.\n");
      return -1;
   }
   
   
	 /* Spectrum Analyzer Center Frequency setting */
   sprintf(strTemp,"%d", (int) (measFreq*1000));
   strcpy(stringinput,":FREQ:CENT ");
   strcat(stringinput,strTemp);
   strcat(stringinput,"MHz");
            								printf("\"%s\"\n",stringinput);	 ////     
   status = viWrite (instrSpecAna, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
   if (status < VI_SUCCESS)  
   {
      printf("Error writing to the device\n");
      return -1;
   }
   
	/* Spectrum Analyzer Span setting */
   sprintf(strTemp,"%d",(int) (measFreq*200));
   strcpy(stringinput,":FREQ:SPAN ");
   strcat(stringinput,strTemp);
   strcat(stringinput,"MHz");
            								printf("\"%s\"\n",stringinput);	 ////     
   status = viWrite (instrSpecAna, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
   if (status < VI_SUCCESS)  
   {
      printf("Error writing to the device\n");
      return -1;
   }
   
	 /* Spectrum Analyzer Reference Level setting */
   sprintf(strTemp,"%d",(int) ((pwrStop+amp_gain[0]-measAtten)/10+0.5)*10);
   strcpy(stringinput,":DISP:WIND:TRAC:Y:RLEV ");
   strcat(stringinput,strTemp);
   strcat(stringinput,"dBm");
            								printf("\"%s\"\n",stringinput);	 ////     
   status = viWrite (instrSpecAna, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
   if (status < VI_SUCCESS)  
   {
      printf("Error writing to the device\n");
      return -1;
   }  
   
	 /* Spectrum Analyzer Y Scale [dB/div]  setting */
   if ((pwrStop-pwrStart)<50) strcpy(strTemp,"5");
   else if ((pwrStop-pwrStart)<100) strcpy(strTemp,"10");
   else strcpy(strTemp,"20");
   strcpy(stringinput,":DISP:WIND:TRAC:Y:PDIV ");
   strcat(stringinput,strTemp);
   strcat(stringinput,"dB");
            								printf("\"%s\"\n",stringinput);	 ////     
   status = viWrite (instrSpecAna, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
   if (status < VI_SUCCESS)  
   {
      printf("Error writing to the device\n");
      return -1;
   }  
   
   
   
	/* Signal Generator Frequency setting */
   sprintf(strTemp,"%d", (int) (measFreq*1000));
   strcpy(stringinput,":FREQ ");
   strcat(stringinput,strTemp);
   strcat(stringinput,"MHz");
            								printf("\"%s\"\n",stringinput);	 ////     
   status = viWrite (instrSigGen, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
   if (status < VI_SUCCESS)  
   {
      printf("Error writing to the device\n");
      return -1;
   }

   for(i=0;i<amp_num;i++){
		if(abs(amp_y[i] - pwrStart + i*ampStep) < 0.05) {
			break;
		}
	}
	
	printf("first: %d, %f", i, amp_x[i]);
 	/* Signal Generator Power setting */
	if(!powerSetting(amp_x[i])) return -1;
            								printf("\"%s\"\n",stringinput);	 ////     
   status = viWrite (instrSigGen, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
   if (status < VI_SUCCESS)  
   {
      printf("Error writing to the device\n");
      return -1;
   }

	 /* Signal Generator Power ON */
   strcpy(stringinput,":OUTP ON");
            								printf("\"%s\"\n",stringinput);	 ////     
   status = viWrite (instrSigGen, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
   if (status < VI_SUCCESS)  
   {
      printf("Error writing to the device\n");
      return -1;
   }   
   
	/* Spectrum Analyzer Marker ON at center frequency*/
   strcpy(stringinput,":CALC:MARK:AOFF\n :CALC:MARK:MODE POS\n :CALC:MARK:X ");
   sprintf(strTemp,"%d", (int) (measFreq*1000));
   strcat(stringinput,strTemp);
   strcat(stringinput,"MHz");
            								printf("\"%s\"\n",stringinput);	 ////     
   status = viWrite (instrSpecAna, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
   if (status < VI_SUCCESS)  
   {
      printf("Error writing to the device\n");
      return -1;
   }

	/* Spectrum Analyzer Average ON */
   strcpy(stringinput,":AVER ON;:AVER:COUNT 16");
            								printf("\"%s\"\n",stringinput);	 ////     
   status = viWrite (instrSpecAna, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
   if (status < VI_SUCCESS)  
   {
      printf("Error writing to the device\n");
      return -1;
   }

   
   /* Aerage Reset		 */
    strcpy(stringinput,":SENS:AVER:CLE");
    status = viWrite (instrSpecAna, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
   if (status < VI_SUCCESS)  
   {
      printf("Error writing to the device\n");
      return -1;
   }

      Delay(waitingTime);

   
	/* Spectrum Analyzer Read the marked value */
   strcpy(stringinput,":CALC:MARK:Y?");
            								printf("\"%s\"\n",stringinput);	 ////    
   Delay(waitingTime);											
   status = viWrite (instrSpecAna, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
   if (status < VI_SUCCESS)  
   {
      printf("Error writing to the device\n");
      return -1;
   }

   status = viRead (instrSpecAna, buffer, 100, &retCount);
   if (status < VI_SUCCESS) 
   {
       printf("Error reading a response from the device\n");
       return -1;
   }
            								printf("\"%s\"\n",buffer);	 //// 
            								printf("length=%d\n",retCount);	 //// 
	x[0]=pwrStart+amp_gain[0];
   	y[0]=atof(buffer)+measAtten;
   
            								printf("value=%f\n",y[0]);	 ////
	
	if (cancel == 1) return 0;
	
	gain[0] = y[0] - x[0];
	int ai = 0;
	
	
		 /* Graph Set X-Y range */
		divValue=1;
	if ((pwrStop-pwrStart)>10) divValue=2;
	if ((pwrStop-pwrStart)>20) divValue=5;
	if ((pwrStop-pwrStart)>50) divValue=10;
	if ((pwrStop-pwrStart)>100) divValue=20;

	SetAxisScalingMode(ConTabPanelHandle, TABCON_GRAPH, VAL_BOTTOM_XAXIS, VAL_MANUAL,
		((int)((amp_y[0]) / divValue)-1)*divValue,
		((int)((amp_y[amp_num-1])/ divValue)+1)*divValue);											
										
	SetAxisScalingMode(ConTabPanelHandle, TABCON_GRAPH, VAL_LEFT_YAXIS, VAL_MANUAL,
		((int)((y[0]) / divValue)-1)*divValue,
		((int)((amp_y[amp_num-1]) / divValue)+1)*divValue);											

	SetCtrlAttribute (ConTabPanelHandle, TABCON_GRAPH, ATTR_XLABEL_VISIBLE, 1);
	SetCtrlAttribute (ConTabPanelHandle, TABCON_GRAPH, ATTR_YLABEL_VISIBLE, 1);

		 /* Spectrum Analyzer Reference Level setting */
		sprintf(strTemp,"%d",(int) ((y[0]+pwrStop-pwrStart)/10+0.5)*10);
		strcpy(stringinput,":DISP:WIND:TRAC:Y:RLEV ");
		strcat(stringinput,strTemp);
		strcat(stringinput,"dBm");
		            								printf("\"%s\"\n",stringinput);	 ////     
		status = viWrite (instrSpecAna, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
		if (status < VI_SUCCESS)  
		{
		   printf("Error writing to the device\n");
		   return -1;
		}  


		DeleteGraphPlot(ConTabPanelHandle, TABCON_GRAPH,-1,VAL_IMMEDIATE_DRAW);

		measPower = pwrStart;
		numPoints = 0;
		
		printf("%d, %f, %d", i, ((pwrStop - pwrStart)/pwrStep), cancel);
		
		while(i < amp_num)	
		{
			if (cancel == 1) return 0;
		 /* Signal Generator Power setting */
		   if(!powerSetting(amp_x[i])) return -1;   
		            								printf("\"%s\"\n",stringinput);	 ////     
		   status = viWrite (instrSigGen, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
		   if (status < VI_SUCCESS)  
		   {
		      printf("Error writing to the device\n");
		      return -1;
		   }
   
		/* Aerage Reset		 */
		    strcpy(stringinput,":SENS:AVER:CLE");
		    status = viWrite (instrSpecAna, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
		   if (status < VI_SUCCESS)  
		   {
		      printf("Error writing to the device\n");
		      return -1;
		   }

		 /* Spectrum Analyzer Read the marked value */

		    strcpy(stringinput,":CALC:MARK:Y?");
		            								printf("\"%s\"\n",stringinput);	 ////     
		   Delay(waitingTime);
		   status = viWrite (instrSpecAna, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
		   if (status < VI_SUCCESS)  
		   {
		      printf("Error writing to the device\n");
		      return -1;
		   }

		   status = viRead (instrSpecAna, buffer, 100, &retCount);
		   if (status < VI_SUCCESS) 
		   {
		       printf("Error reading a response from the device\n");
		       return -1;
		   }
			
		   x[i]= amp_y[i];
		   y[i]= atof(buffer);
		   gain[i]= y[i]-x[i];
		            								printf("measure gen:%f, x:%f, y:%f, gain:%f\n",amp_x[i],x[i],y[i], gain[i]);	 ////     
   		   
		   
		   PlotXY(ConTabPanelHandle, TABCON_GRAPH, &x[i], &y[i], 1, 
			   VAL_DOUBLE, VAL_DOUBLE, 
			   VAL_CONNECTED_POINTS, VAL_SOLID_SQUARE,
			   VAL_SOLID, 1, VAL_YELLOW);
		   PlotXY(ConTabPanelHandle, TABCON_GRAPH, &x[i], &gain[i], 1, 
			   VAL_DOUBLE, VAL_DOUBLE, 
			   VAL_CONNECTED_POINTS, VAL_SOLID_SQUARE,
			   VAL_SOLID, 1, VAL_BLUE);
		    SetCtrlVal (ConTabPanelHandle, TABCON_P_IN, x[i]);				   /////// 에드모텍 수정 -를 +로 수정함.
			SetCtrlVal (ConTabPanelHandle, TABCON_P_OUT, y[i]);
			SetCtrlVal (ConTabPanelHandle, TABCON_GAIN, gain[i]);
		   
   
		   i++;
		   numPoints++;
   
		}

		SetCtrlAttribute (ConTabPanelHandle, TABCON_GRAPH, ATTR_XLABEL_VISIBLE, 1);
		SetCtrlAttribute (ConTabPanelHandle, TABCON_GRAPH, ATTR_YLABEL_VISIBLE, 1);

			            					printf("numPoints:%d\n",numPoints);	 ////     
		for ( i=1;i<numPoints;++i)
		{
			if (cancel == 1) return 0;
			p1dBIn=amp_x[i];
			p1dBx = x[i];
			p1dBOut=y[i];
			            					printf("i=%d, gain[0]:%f, gain:%f\n",i, gain[0],y[i]-x[i]);	 ////     

			if ((gain[0]-gain[i]) >= 1)
			{
				pwrHigh=amp_x[i];
				pwrLow=amp_x[i-1];
		
				while(abs((int)((gain[0]-gain[i]-1)+0.5)) >= 1)
				{
					if (cancel == 1) return 0;
					if( gain[0]-gain[i] > 1) 
					{
						pwrHigh=p1dBIn;
						p1dBIn=(pwrLow+p1dBIn)/2 ;
					} else
					{
						pwrLow=p1dBIn;
						p1dBIn=(pwrHigh+p1dBIn)/2 ;
					}
					
					if(!powerSetting(p1dBIn)) return -1;
					
		            								printf("\"%s\"\n",stringinput);	 ////     
		   			status = viWrite (instrSigGen, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
		   			if (status < VI_SUCCESS)  
		   			{
		      			printf("Error writing to the device\n");
		      			return -1;
		   			}

				   /* Aerage Reset		 */
				    strcpy(stringinput,":SENS:AVER:CLE");
				    status = viWrite (instrSpecAna, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
				   if (status < VI_SUCCESS)  
				   {
				      printf("Error writing to the device\n");
				      return -1;
				   }
			
					/* Spectrum Analyzer Read the marked value */
   
		   			strcpy(stringinput,":CALC:MARK:Y?");
		            								printf("\"%s\"\n",stringinput);	 ////     
		   			Delay(waitingTime);
		   			status = viWrite (instrSpecAna, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
		   			if (status < VI_SUCCESS)  
		   			{
		      			printf("Error writing to the device\n");
		      			return -1;
		   			}

		   			status = viRead (instrSpecAna, buffer, 100, &retCount);
		   			if (status < VI_SUCCESS) 
		   			{
		       			printf("Error reading a response from the device\n");
		       			return -1;
		  			}
		   			p1dBOut=atof(buffer);
		            								printf("measure%f,%f\n",p1dBIn,p1dBOut-p1dBIn);	 ////     
   					
		   			PlotXY(ConTabPanelHandle, TABCON_GRAPH,&p1dBIn, &p1dBOut, 1, 
			   			VAL_DOUBLE, VAL_DOUBLE, 
			   			VAL_CONNECTED_POINTS, VAL_SOLID_SQUARE,
			   			VAL_SOLID, 1, VAL_WHITE);
			
				}
				
				//SetCtrlVal (ConTabPanelHandle, TABCON_P1dB_IN, p1dBIn);		
				SetCtrlVal (ConTabPanelHandle, TABCON_P1dB_OUT, p1dBx);
				PlotXY(ConTabPanelHandle, TABCON_GRAPH, &p1dBIn, &p1dBOut, 1, 
			   			VAL_DOUBLE, VAL_DOUBLE, 
			   			VAL_CONNECTED_POINTS, VAL_SOLID_SQUARE,
			   			VAL_SOLID, 1, VAL_RED);
				
				
				break;
			}
		}
		
		flat_leak = y[0];
		for(i=1;i<numPoints;++i) {
			if(flat_leak < y[i]) {
				flat_leak = y[i];
			}
		}


		SetCtrlVal (ConTabPanelHandle, TABCON_FLAT_LEAK, flat_leak);
	
	
	return 0;
}

int CVICALLBACK MAINTAB (int panel, int control, int event,
						 void *callbackData, int eventData1, int eventData2)
{
	return 0;
}

int CVICALLBACK ManSaveBT (int panel, int control, int event,
						   void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			break;
	}
	return 0;
}

int CVICALLBACK ManStartBT (int panel, int control, int event,
							void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			break;
	}
	return 0;
}

int CVICALLBACK ManCancelBT (int panel, int control, int event,
							 void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			break;
	}
	return 0;
}

int CVICALLBACK manAmpCheck (int panel, int control, int event,
							 void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			break;
	}
	return 0;
}

int CVICALLBACK powerSet (int panel, int control, int event,
						  void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			break;
	}
	return 0;
}

int CVICALLBACK output (int panel, int control, int event,
						void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			break;
	}
	return 0;
}

int CVICALLBACK refApplyCallback (int panel, int control, int event,
								  void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:
			GetCtrlVal(RefTabPanelHandle, TABREF_REFLVLOFFSET, &reflvloffset);
			strcpy(stringinput,"DISP:WIND:TRAC:Y:RLEV:OFFS ");
		   sprintf(strTemp,"%f", reflvloffset);
		   strcat(stringinput,strTemp);
		            								printf("ref lvl offset setting: \"%f\"\n",reflvloffset);     
		   status = viWrite (instrSpecAna, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
		   if (status < VI_SUCCESS)  
		   {
		      printf("Error writing to the device\n");
		      return -1;
		   }
		   
		   strcpy(stringinput,"DISP:WIND:TRAC:Y:RLEV:OFFS?");
		   Delay(waitingTime);											
		   status = viWrite (instrSpecAna, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
		   if (status < VI_SUCCESS)  
		   {
		      printf("Error writing to the device\n");
		      return -1;
		   }

		   status = viRead (instrSpecAna, buffer, 100, &retCount);
		   if (status < VI_SUCCESS) 
		   {
		       printf("Error reading a response from the device\n");
		       return -1;
		   }
		            								printf("\"%s\"\n",buffer);	 //// 
		            								printf("length=%d\n",retCount);	 ////
			break;
	}
	return 0;
}

int CVICALLBACK refStartCallback (int panel, int control, int event,
								  void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:
			GetCtrlVal(RefTabPanelHandle, TABREF_STARTPOW, &pwrStart);
			GetCtrlVal(RefTabPanelHandle, TABREF_FREQ, &measFreq);
			
			status = viOpen (defaultRM, descriptionSpecAna, VI_NULL, VI_NULL, &instrSpecAna);
		   if (status < VI_SUCCESS)  
		   {
		      printf ("Cannot open a session to the device.\n");
		      return -1;
		   }
		   status = viOpen (defaultRM, descriptionSigGen, VI_NULL, VI_NULL, &instrSigGen);
		   if (status < VI_SUCCESS)  
		   {
		      printf ("Cannot open a session to the device.\n");
		      return -1;
		   }
   
   
		 /* Spectrum Analyzer Center Frequency setting */
		   sprintf(strTemp,"%d", (int) (measFreq*1000));
		   strcpy(stringinput,":FREQ:CENT ");
		   strcat(stringinput,strTemp);
		   strcat(stringinput,"MHz");
		            								printf("\"%s\"\n",stringinput);	 ////     
		   status = viWrite (instrSpecAna, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
		   if (status < VI_SUCCESS)  
		   {
		      printf("Error writing to the device\n");
		      return -1;
		   }
   
			/* Spectrum Analyzer Span setting */
		   sprintf(strTemp,"%d",(int) (measFreq*200));
		   strcpy(stringinput,":FREQ:SPAN ");
		   strcat(stringinput,strTemp);
		   strcat(stringinput,"MHz");
		            								printf("\"%s\"\n",stringinput);	 ////     
		   status = viWrite (instrSpecAna, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
		   if (status < VI_SUCCESS)  
		   {
		      printf("Error writing to the device\n");
		      return -1;
		   }
   
			 /* Spectrum Analyzer Reference Level setting */
		   sprintf(strTemp,"%d",(int) (pwrStart/10+0.5)*10);
		   strcpy(stringinput,":DISP:WIND:TRAC:Y:RLEV ");
		   strcat(stringinput,strTemp);
		   strcat(stringinput,"dBm");
		            								printf("\"%s\"\n",stringinput);	 ////     
		   status = viWrite (instrSpecAna, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
		   if (status < VI_SUCCESS)  
		   {
		      printf("Error writing to the device\n");
		      return -1;
		   }  
   
			 /* Spectrum Analyzer Y Scale [dB/div]  setting */
		   strcpy(strTemp,"5");
		   strcpy(stringinput,":DISP:WIND:TRAC:Y:PDIV ");
		   strcat(stringinput,strTemp);
		   strcat(stringinput,"dB");
		            								printf("\"%s\"\n",stringinput);	 ////     
		   status = viWrite (instrSpecAna, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
		   if (status < VI_SUCCESS)  
		   {
		      printf("Error writing to the device\n");
		      return -1;
		   }  
   
   
   
			/* Signal Generator Frequency setting */
		   sprintf(strTemp,"%d", (int) (measFreq*1000));
		   strcpy(stringinput,":FREQ ");
		   strcat(stringinput, strTemp);
		   strcat(stringinput,"MHz");
		            								printf("\"%s\"\n",stringinput);	 ////     
		   status = viWrite (instrSigGen, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
		   if (status < VI_SUCCESS)  
		   {
		      printf("Error writing to the device\n");
		      return -1;
		   }

		 	/* Signal Generator Power setting */
			if(!powerSetting(pwrStart)) return -1;
		            								printf("\"%s\"\n",stringinput);	 ////     
		   status = viWrite (instrSigGen, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
		   if (status < VI_SUCCESS)  
		   {
		      printf("Error writing to the device\n");
		      return -1;
		   }

			 /* Signal Generator Power ON */
		   strcpy(stringinput,":OUTP ON");
		            								printf("\"%s\"\n",stringinput);	 ////     
		   status = viWrite (instrSigGen, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
		   if (status < VI_SUCCESS)  
		   {
		      printf("Error writing to the device\n");
		      return -1;
		   }   
   
			/* Spectrum Analyzer Marker ON at center frequency*/
		   strcpy(stringinput,":CALC:MARK:AOFF\n :CALC:MARK:MODE POS\n :CALC:MARK:X ");
		   sprintf(strTemp,"%d", (int) (measFreq*1000));
		   strcat(stringinput,strTemp);
		   strcat(stringinput,"MHz");
		            								printf("\"%s\"\n",stringinput);	 ////     
		   status = viWrite (instrSpecAna, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
		   if (status < VI_SUCCESS)  
		   {
		      printf("Error writing to the device\n");
		      return -1;
		   }

			/* Spectrum Analyzer Average ON */
		   strcpy(stringinput,":AVER ON;:AVER:COUNT 16");
		            								printf("\"%s\"\n",stringinput);	 ////     
		   status = viWrite (instrSpecAna, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
		   if (status < VI_SUCCESS)  
		   {
		      printf("Error writing to the device\n");
		      return -1;
		   }

   
		   /* Aerage Reset		 */
		    strcpy(stringinput,":SENS:AVER:CLE");
		    status = viWrite (instrSpecAna, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
		   if (status < VI_SUCCESS)  
		   {
		      printf("Error writing to the device\n");
		      return -1;
		   }

		      Delay(waitingTime);

   
			/* Spectrum Analyzer Read the marked value */
		   strcpy(stringinput,":CALC:MARK:Y?");
		            								printf("\"%s\"\n",stringinput);	 ////    
		   Delay(waitingTime);											
		   status = viWrite (instrSpecAna, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
		   if (status < VI_SUCCESS)  
		   {
		      printf("Error writing to the device\n");
		      return -1;
		   }

		   status = viRead (instrSpecAna, buffer, 100, &retCount);
		   if (status < VI_SUCCESS) 
		   {
		       printf("Error reading a response from the device\n");
		       return -1;
		   }
		            								printf("\"%s\"\n",buffer);	 //// 
		            								printf("length=%d\n",retCount);	 //// 
			x[0]=pwrStart;
		   	y[0]=atof(buffer);
		
			SetCtrlVal (RefTabPanelHandle, TABREF_OUTPUT, y[0]);
	   		SetCtrlVal (RefTabPanelHandle, TABREF_REFLVLOFFSET, x[0]-y[0]);
		
			/* Signal Generator Power ON */
			strcpy(stringinput,":OUTP OFF");
			            								printf("\"%s\"\n",stringinput);	 ////     
			status = viWrite (instrSigGen, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
			if (status < VI_SUCCESS)  
			{
			   printf("Error writing to the device\n");
			   return -1;
			}
    
			break;
	}
	return 0;
}

int CVICALLBACK refSaveBT (int panel, int control, int event,
						   void *callbackData, int eventData1, int eventData2)
{
	FILE* saveFile;
	char fileName[MAX_PATHNAME_LEN];
	char filePath[MAX_PATHNAME_LEN];
	char msg[100];
	switch (event)
	{
		case EVENT_COMMIT:
			time (&tt);
			tm = localtime (&tt);
			z = asctime (tm);
			
			DebugPrintf ("System time: %s\n", z);
			strcpy(filePath,"reflvloffset\\");
			GetCtrlVal(panel, TABREF_SAVE_FILENAME, fileName);
			strcat(filePath, fileName);
			saveFile = OpenFile (filePath, VAL_WRITE_ONLY, VAL_OPEN_AS_IS, VAL_ASCII);
			
			sprintf (msg, "#reflvloffset\n");
			WriteLine (saveFile, msg, -1);
			sprintf (msg, "#measure time: %s\n", z);
			WriteLine (saveFile, msg, -1);
			sprintf (msg, "@power_start:%f\n", pwrStart);
			WriteLine (saveFile, msg, -1);
			sprintf (msg, "@frequecy:%f\n", measFreq);
			WriteLine (saveFile, msg, -1);
			
			sprintf (msg, "*output:%f\n*reflvloffset:%f\n", x[0], reflvloffset);
			WriteLine (saveFile, msg, -1);
			CloseFile (saveFile);
			
			MessagePopup("success", "save success");
			break;
	}
	return 0;
}

int CVICALLBACK AmpStartBT (int panel, int control, int event,
							void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:
			SetCtrlAttribute (panel, TABAMP_CANCEL, ATTR_DIMMED, 0);
			SetCtrlAttribute (panel, control, ATTR_DIMMED, 1);
			CmtScheduleThreadPoolFunction (DEFAULT_THREAD_POOL_HANDLE, ampThread, NULL, &AmpThreadFunctionID);//schedule the thread passing our functions
			
			CmtWaitForThreadPoolFunctionCompletion(DEFAULT_THREAD_POOL_HANDLE, AmpThreadFunctionID, OPT_TP_PROCESS_EVENTS_WHILE_WAITING); //wait for the thread to complete
			CmtReleaseThreadPoolFunctionID(DEFAULT_THREAD_POOL_HANDLE, AmpThreadFunctionID); //release thread resources.
			
			 /* Signal Generator Power ON */
			strcpy(stringinput,":OUTP OFF");
			            								printf("\"%s\"\n",stringinput);	 ////     
			status = viWrite (instrSigGen, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
			if (status < VI_SUCCESS)  
			{
			   printf("Error writing to the device\n");
			   return -1;
			}
			SetCtrlAttribute (panel, TABCON_CANCEL, ATTR_DIMMED, 1);
			SetCtrlAttribute (panel, control, ATTR_DIMMED, 0);
			break;
	}
	return 0;
}

int CVICALLBACK AmpCancelBT (int panel, int control, int event,
							 void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:
			if (cancel == 0) {
				cancel = 1;
			}
			SetCtrlAttribute (panel, control, ATTR_DIMMED, 1);
			break;
	}
	return 0;
}

int CVICALLBACK AmpSaveBT (int panel, int control, int event,
						   void *callbackData, int eventData1, int eventData2)
{
	FILE* saveFile;
	char fileName[MAX_PATHNAME_LEN];
	char filePath[MAX_PATHNAME_LEN];
	char msg[100];
	switch (event)
	{
		case EVENT_COMMIT:
			time (&tt);
			tm = localtime (&tt);
			z = asctime (tm);
			
			DebugPrintf ("System time: %s\n", z);
			strcpy(filePath,"amp\\");
			GetCtrlVal(panel, TABAMP_SAVE_FILENAME, fileName);
			strcat(filePath, fileName);
			saveFile = OpenFile (filePath, VAL_WRITE_ONLY, VAL_OPEN_AS_IS, VAL_ASCII);
			
			sprintf (msg, "#reflvloffset\n");
			WriteLine (saveFile, msg, -1);
			sprintf (msg, "#measure time: %s\n", z);
			WriteLine (saveFile, msg, -1);
			sprintf (msg, "@power_start:%f\n", pwrStart);
			WriteLine (saveFile, msg, -1);
			sprintf (msg, "@power_stop:%f\n", pwrStop);
			WriteLine (saveFile, msg, -1);
			sprintf (msg, "@frequecy:%f\n", measFreq);
			WriteLine (saveFile, msg, -1);
			
			sprintf (msg, "*output:%f\n*reflvloffset:%f\n", x[0], reflvloffset);
			WriteLine (saveFile, msg, -1);
			
			int i=0;
			for(i=0; i<numPoints; i++){
				sprintf (msg, "^P.in:%f,P.out:%f,gain:%f\n", x[i], y[i], gain[i]);
				WriteLine (saveFile, msg, -1);
			}
			
			CloseFile (saveFile);
			
			MessagePopup("success", "save success");
			break;
	}
	return 0;
}


static int CVICALLBACK ampThread (void *functionData){
	cancel = 0;
	measPower = -50;
	
	GetCtrlVal(AmpTabPanelHandle, TABAMP_FREQ,&measFreq);
	GetCtrlVal(AmpTabPanelHandle,TABAMP_STEP,&pwrStep);
	GetCtrlVal(AmpTabPanelHandle,TABAMP_STARTPOW,&pwrStart);
	GetCtrlVal(AmpTabPanelHandle,TABAMP_STOPPOW,&pwrStop);
	
	status = viOpen (defaultRM, descriptionSpecAna, VI_NULL, VI_NULL, &instrSpecAna);
   if (status < VI_SUCCESS)  
   {
      printf ("Cannot open a session to the device.\n");
      return -1;
   }
   status = viOpen (defaultRM, descriptionSigGen, VI_NULL, VI_NULL, &instrSigGen);
   if (status < VI_SUCCESS)  
   {
      printf ("Cannot open a session to the device.\n");
      return -1;
   }
   						  
   
	 /* Spectrum Analyzer Center Frequency setting */
   sprintf(strTemp,"%d", (int) (measFreq*1000));
   strcpy(stringinput,":FREQ:CENT ");
   strcat(stringinput,strTemp);
   strcat(stringinput,"MHz");
            								printf("\"%s\"\n",stringinput);	 ////     
   status = viWrite (instrSpecAna, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
   if (status < VI_SUCCESS)  
   {
      printf("Error writing to the device\n");
      return -1;
   }
   
	/* Spectrum Analyzer Span setting */
   sprintf(strTemp,"%d",(int) (measFreq*200));
   strcpy(stringinput,":FREQ:SPAN ");
   strcat(stringinput,strTemp);
   strcat(stringinput,"MHz");
            								printf("\"%s\"\n",stringinput);	 ////     
   status = viWrite (instrSpecAna, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
   if (status < VI_SUCCESS)  
   {
      printf("Error writing to the device\n");
      return -1;
   }
   
	 /* Spectrum Analyzer Reference Level setting */
   sprintf(strTemp,"%d",(int) ((pwrStop)/10+0.5)*10);
   strcpy(stringinput,":DISP:WIND:TRAC:Y:RLEV ");
   strcat(stringinput,strTemp);
   strcat(stringinput,"dBm");
            								printf("\"%s\"\n",stringinput);	 ////     
   status = viWrite (instrSpecAna, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
   if (status < VI_SUCCESS)  
   {
      printf("Error writing to the device\n");
      return -1;
   }  
   
	 /* Spectrum Analyzer Y Scale [dB/div]  setting */
   //if ((pwrStop-pwrStart)<50) strcpy(strTemp,"5");
   //else if ((pwrStop-pwrStart)<100) strcpy(strTemp,"10");
   //else strcpy(strTemp,"20");
   strcpy(strTemp,"10");
   strcpy(stringinput,":DISP:WIND:TRAC:Y:PDIV ");
   strcat(stringinput,strTemp);
   strcat(stringinput,"dB");
            								printf("\"%s\"\n",stringinput);	 ////     
   status = viWrite (instrSpecAna, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
   if (status < VI_SUCCESS)  
   {
      printf("Error writing to the device\n");
      return -1;
   }  
   
   
   
	/* Signal Generator Frequency setting */
   sprintf(strTemp,"%d", (int) (measFreq*1000));
   strcpy(stringinput,":FREQ ");
   strcat(stringinput,strTemp);
   strcat(stringinput,"MHz");
            								printf("\"%s\"\n",stringinput);	 ////     
   status = viWrite (instrSigGen, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
   if (status < VI_SUCCESS)  
   {
      printf("Error writing to the device\n");
      return -1;
   }

 	/* Signal Generator Power setting */
	if(!powerSetting(measPower)) return -1;
            								printf("\"%s\"\n",stringinput);	 ////     
   status = viWrite (instrSigGen, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
   if (status < VI_SUCCESS)  
   {
      printf("Error writing to the device\n");
      return -1;
   }

	 /* Signal Generator Power ON */
   strcpy(stringinput,":OUTP ON");
            								printf("\"%s\"\n",stringinput);	 ////     
   status = viWrite (instrSigGen, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
   if (status < VI_SUCCESS)  
   {
      printf("Error writing to the device\n");
      return -1;
   }   
   
	/* Spectrum Analyzer Marker ON at center frequency*/
   strcpy(stringinput,":CALC:MARK:AOFF\n :CALC:MARK:MODE POS\n :CALC:MARK:X ");
   sprintf(strTemp,"%d", (int) (measFreq*1000));
   strcat(stringinput,strTemp);
   strcat(stringinput,"MHz");
            								printf("\"%s\"\n",stringinput);	 ////     
   status = viWrite (instrSpecAna, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
   if (status < VI_SUCCESS)  
   {
      printf("Error writing to the device\n");
      return -1;
   }

	/* Spectrum Analyzer Average ON */
   strcpy(stringinput,":AVER ON;:AVER:COUNT 16");
            								printf("\"%s\"\n",stringinput);	 ////     
   status = viWrite (instrSpecAna, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
   if (status < VI_SUCCESS)  
   {
      printf("Error writing to the device\n");
      return -1;
   }

   
   /* Aerage Reset		 */
    strcpy(stringinput,":SENS:AVER:CLE");
    status = viWrite (instrSpecAna, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
   if (status < VI_SUCCESS)  
   {
      printf("Error writing to the device\n");
      return -1;
   }

      Delay(waitingTime);

   
	/* Spectrum Analyzer Read the marked value */
   strcpy(stringinput,":CALC:MARK:Y?");
            								printf("\"%s\"\n",stringinput);	 ////    
   Delay(waitingTime);											
   status = viWrite (instrSpecAna, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
   if (status < VI_SUCCESS)  
   {
      printf("Error writing to the device\n");
      return -1;
   }

   status = viRead (instrSpecAna, buffer, 100, &retCount);
   if (status < VI_SUCCESS) 
   {
       printf("Error reading a response from the device\n");
       return -1;
   }
            								printf("\"%s\"\n",buffer);	 //// 
            								printf("length=%d\n",retCount);	 //// 
	
	double tmpx, tmpy ,tmpResult=999;
	tmpx=measPower;
   	tmpy=atof(buffer);
   
            								printf("value=%f\n", tmpy);	 ////
	
	
	
	while(tmpy > (pwrStart+0.05) || tmpy < (pwrStart-0.05))	{
			if (cancel == 1) return 0;
			tmpx = tmpx - (tmpy - pwrStart);	
		 /* Signal Generator Power setting */
		 if(!powerSetting(tmpx)) return -1;
		            								printf("\"%s\"\n",stringinput);	 ////     
		   status = viWrite (instrSigGen, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
		   if (status < VI_SUCCESS)  
		   {
		      printf("Error writing to the device\n");
		      return -1;
		   }
   
		/* Aerage Reset		 */
		    strcpy(stringinput,":SENS:AVER:CLE");
		    status = viWrite (instrSpecAna, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
		   if (status < VI_SUCCESS)  
		   {
		      printf("Error writing to the device\n");
		      return -1;
		   }

		 /* Spectrum Analyzer Read the marked value */

		    strcpy(stringinput,":CALC:MARK:Y?");
		            								printf("\"%s\"\n",stringinput);	 ////     
		   Delay(waitingTime);
		   status = viWrite (instrSpecAna, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
		   if (status < VI_SUCCESS)  
		   {
		      printf("Error writing to the device\n");
		      return -1;
		   }

		   status = viRead (instrSpecAna, buffer, 100, &retCount);
		   if (status < VI_SUCCESS) 
		   {
		       printf("Error reading a response from the device\n");
		       return -1;
		   }
		   
		    tmpy = atof(buffer);
		   
		   	SetCtrlVal (AmpTabPanelHandle, TABAMP_P_IN, tmpx);				   /////// 에드모텍 수정 -를 +로 수정함.
			SetCtrlVal (AmpTabPanelHandle, TABAMP_P_OUT, tmpy);
			SetCtrlVal (AmpTabPanelHandle, TABAMP_GAIN, tmpy-tmpx);
   
		}
		
		 printf("0dbm = %f, %f\n", tmpx, tmpy);
		 x[0] = tmpx;
		 y[0] = tmpy;
		 gain[0] = tmpy - tmpx;
		//Graph setting
		divValue=1;
	if ((pwrStop-pwrStart)>10) divValue=2;
	if ((pwrStop-pwrStart)>20) divValue=5;
	if ((pwrStop-pwrStart)>50) divValue=10;
	if ((pwrStop-pwrStart)>100) divValue=20;

	SetAxisScalingMode(AmpTabPanelHandle, TABAMP_GRAPH, VAL_BOTTOM_XAXIS, VAL_MANUAL,
		((int)((x[0]) / divValue)-1)*divValue,
		((int)((x[0] + (pwrStop-pwrStart))/ divValue)+1)*divValue);											
										
	SetAxisScalingMode(AmpTabPanelHandle, TABAMP_GRAPH, VAL_LEFT_YAXIS, VAL_MANUAL,
		((int)((pwrStart) / divValue)-1)*divValue,
		((int)((pwrStop) / divValue)+1)*divValue);											

	SetCtrlAttribute (AmpTabPanelHandle, TABAMP_GRAPH, ATTR_XLABEL_VISIBLE, 1);
	SetCtrlAttribute (AmpTabPanelHandle, TABAMP_GRAPH, ATTR_YLABEL_VISIBLE, 1); 
	
	PlotXY(AmpTabPanelHandle, TABAMP_GRAPH, &x[0], &y[0], 1, 
			VAL_DOUBLE, VAL_DOUBLE, 
			VAL_CONNECTED_POINTS, VAL_SOLID_SQUARE,
			VAL_SOLID, 1, VAL_YELLOW);
	
	int i=1;
	double goal = pwrStart;
	while(goal < pwrStop) {
		if (cancel == 1) return 0;
		goal = goal + pwrStep;
		tmpx = tmpx + pwrStep;
		printf("goal: %f", goal);
		/* Signal Generator Power setting */
		if(!powerSetting(tmpx)) return -1;	 
		            								printf("\"%s\"\n",stringinput);	 ////     
		   status = viWrite (instrSigGen, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
		   if (status < VI_SUCCESS)  
		   {
		      printf("Error writing to the device\n");
		      return -1;
		   }
		   
		   /* Aerage Reset		 */
		    strcpy(stringinput,":SENS:AVER:CLE");
		    status = viWrite (instrSpecAna, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
		   if (status < VI_SUCCESS)  
		   {
		      printf("Error writing to the device\n");
		      return -1;
		   }

		 /* Spectrum Analyzer Read the marked value */

		    strcpy(stringinput,":CALC:MARK:Y?");
		            								printf("\"%s\"\n",stringinput);	 ////     
		   Delay(waitingTime);
		   status = viWrite (instrSpecAna, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
		   if (status < VI_SUCCESS)  
		   {
		      printf("Error writing to the device\n");
		      return -1;
		   }

		   status = viRead (instrSpecAna, buffer, 100, &retCount);
		   if (status < VI_SUCCESS) 
		   {
		       printf("Error reading a response from the device\n");
		       return -1;
		   }
		   
		    tmpy = atof(buffer);
			SetCtrlVal (AmpTabPanelHandle, TABAMP_P_IN, tmpx);				   /////// 에드모텍 수정 -를 +로 수정함.
			SetCtrlVal (AmpTabPanelHandle, TABAMP_P_OUT, tmpy);
			SetCtrlVal (AmpTabPanelHandle, TABAMP_GAIN, tmpy-tmpx);
			
		while(tmpy > (goal + 0.05) || tmpy < (goal - 0.05))	{
			if (cancel == 1) return 0;
			tmpx = tmpx + (goal - tmpy) / 2;	
		 /* Signal Generator Power setting */
		   if(!powerSetting(tmpx)) return -1;
		            								printf("\"%s\"\n",stringinput);	 ////     
		   status = viWrite (instrSigGen, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
		   if (status < VI_SUCCESS)  
		   {
		      printf("Error writing to the device\n");
		      return -1;
		   }

		/* Aerage Reset		 */
		    strcpy(stringinput,":SENS:AVER:CLE");
		    status = viWrite (instrSpecAna, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
		   if (status < VI_SUCCESS)  
		   {
		      printf("Error writing to the device\n");
		      return -1;
		   }

		 /* Spectrum Analyzer Read the marked value */

		    strcpy(stringinput,":CALC:MARK:Y?");
		            								printf("\"%s\"\n",stringinput);	 ////     
		   Delay(waitingTime);
		   status = viWrite (instrSpecAna, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
		   if (status < VI_SUCCESS)  
		   {
		      printf("Error writing to the device\n");
		      return -1;
		   }

		   status = viRead (instrSpecAna, buffer, 100, &retCount);
		   if (status < VI_SUCCESS) 
		   {
		       printf("Error reading a response from the device\n");
		       return -1;
		   }

		    tmpy = atof(buffer);

		   	SetCtrlVal (AmpTabPanelHandle, TABAMP_P_IN, tmpx);				   /////// 에드모텍 수정 -를 +로 수정함.
			SetCtrlVal (AmpTabPanelHandle, TABAMP_P_OUT, tmpy);
			SetCtrlVal (AmpTabPanelHandle, TABAMP_GAIN, tmpy-tmpx);

			
		}
		printf("%d dbm = %f, %f\n", i, tmpx, tmpy);
		x[i] = tmpx;
		y[i] = tmpy;
		gain[i] = tmpy - tmpx;
		PlotXY(AmpTabPanelHandle, TABAMP_GRAPH, &x[i], &y[i], 1, 
				VAL_DOUBLE, VAL_DOUBLE, 
				VAL_CONNECTED_POINTS, VAL_SOLID_SQUARE,
				VAL_SOLID, 1, VAL_YELLOW);
		i++;
	}
	numPoints = i;
	printf("total count : %d", i);
	
	return 0;
}
