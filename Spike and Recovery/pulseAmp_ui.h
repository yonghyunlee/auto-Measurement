/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  MEAS                             1       /* callback function: measPanel */
#define  MEAS_TAB                         2       /* control type: tab, callback function: mainTab */

#define  SETUPPANEL                       2       /* callback function: setupPanel */
#define  SETUPPANEL_SETUPBTN              2       /* control type: command, callback function: setupBtnCallback */
#define  SETUPPANEL_FUNGEN                3       /* control type: ring, callback function: funGenCallback */
#define  SETUPPANEL_PPA                   4       /* control type: ring, callback function: ppaCallback */
#define  SETUPPANEL_FUNGENTEXT            5       /* control type: textMsg, callback function: (none) */
#define  SETUPPANEL_SELECTEDPPA           6       /* control type: textMsg, callback function: (none) */
#define  SETUPPANEL_GERNERATOR            7       /* control type: ring, callback function: siggenCallback */
#define  SETUPPANEL_SELETEDSIGTEXT        8       /* control type: textMsg, callback function: (none) */

     /* tab page panel controls */
#define  REC_TAB_INCIDENT                 2       /* control type: numeric, callback function: (none) */
#define  REC_TAB_RECOVERY                 3       /* control type: numeric, callback function: (none) */
#define  REC_TAB_TEXTMSG                  4       /* control type: textMsg, callback function: (none) */
#define  REC_TAB_TEXTMSG_2                5       /* control type: textMsg, callback function: (none) */
#define  REC_TAB_START_BT                 6       /* control type: command, callback function: recoveryStart */
#define  REC_TAB_CANCEL                   7       /* control type: command, callback function: cancelCallback */
#define  REC_TAB_SAVE_FILENAME            8       /* control type: string, callback function: (none) */
#define  REC_TAB_SPLITTER_2               9       /* control type: splitter, callback function: (none) */
#define  REC_TAB_SETTITLE_3               10      /* control type: textMsg, callback function: (none) */
#define  REC_TAB_SETTITLE_2               11      /* control type: textMsg, callback function: (none) */
#define  REC_TAB_SETTITLE                 12      /* control type: textMsg, callback function: (none) */
#define  REC_TAB_GRAPH                    13      /* control type: graph, callback function: (none) */
#define  REC_TAB_SIG_FREQ                 14      /* control type: numeric, callback function: (none) */
#define  REC_TAB_FUN_DUTY                 15      /* control type: numeric, callback function: (none) */
#define  REC_TAB_FUN_PERIOD               16      /* control type: numeric, callback function: (none) */
#define  REC_TAB_FUN_OFFSET               17      /* control type: numeric, callback function: (none) */
#define  REC_TAB_SPLITTER_4               18      /* control type: splitter, callback function: (none) */
#define  REC_TAB_REC_INPUT                19      /* control type: numeric, callback function: (none) */
#define  REC_TAB_SIG_DB_END               20      /* control type: numeric, callback function: (none) */
#define  REC_TAB_SIG_DB_ST                21      /* control type: numeric, callback function: (none) */
#define  REC_TAB_SPLITTER_3               22      /* control type: splitter, callback function: (none) */
#define  REC_TAB_SPLITTER                 23      /* control type: splitter, callback function: (none) */
#define  REC_TAB_FUN_AMPL                 24      /* control type: numeric, callback function: (none) */

     /* tab page panel controls */
#define  SPIKE_TAB_INCIDENT               2       /* control type: numeric, callback function: (none) */
#define  SPIKE_TAB_SPIKE_ERG              3       /* control type: numeric, callback function: (none) */
#define  SPIKE_TAB_TEXTMSG_5              4       /* control type: textMsg, callback function: (none) */
#define  SPIKE_TAB_PULSE_WIDTH            5       /* control type: numeric, callback function: (none) */
#define  SPIKE_TAB_TEXTMSG_4              6       /* control type: textMsg, callback function: (none) */
#define  SPIKE_TAB_SPIKE                  7       /* control type: numeric, callback function: (none) */
#define  SPIKE_TAB_TEXTMSG                8       /* control type: textMsg, callback function: (none) */
#define  SPIKE_TAB_TEXTMSG_2              9       /* control type: textMsg, callback function: (none) */
#define  SPIKE_TAB_START_BT               10      /* control type: command, callback function: spikeStart */
#define  SPIKE_TAB_CANCEL                 11      /* control type: command, callback function: cancelCallback */
#define  SPIKE_TAB_SAVE_FILENAME          12      /* control type: string, callback function: (none) */
#define  SPIKE_TAB_SPLITTER_2             13      /* control type: splitter, callback function: (none) */
#define  SPIKE_TAB_SETTITLE_2             14      /* control type: textMsg, callback function: (none) */
#define  SPIKE_TAB_SETTITLE               15      /* control type: textMsg, callback function: (none) */
#define  SPIKE_TAB_GRAPH                  16      /* control type: graph, callback function: (none) */
#define  SPIKE_TAB_SIG_FREQ               17      /* control type: numeric, callback function: (none) */
#define  SPIKE_TAB_FUN_DUTY               18      /* control type: numeric, callback function: (none) */
#define  SPIKE_TAB_SIG_DB_END             19      /* control type: numeric, callback function: (none) */
#define  SPIKE_TAB_SIG_DB_ST              20      /* control type: numeric, callback function: (none) */
#define  SPIKE_TAB_FUN_PERIOD             21      /* control type: numeric, callback function: (none) */
#define  SPIKE_TAB_FUN_OFFSET             22      /* control type: numeric, callback function: (none) */
#define  SPIKE_TAB_SPLITTER               23      /* control type: splitter, callback function: (none) */
#define  SPIKE_TAB_FUN_AMPL               24      /* control type: numeric, callback function: (none) */
#define  SPIKE_TAB_SPLITTER_3             25      /* control type: splitter, callback function: (none) */


     /* Control Arrays: */

          /* (no control arrays in the resource file) */


     /* Menu Bars, Menus, and Menu Items: */

          /* (no menu bars in the resource file) */


     /* Callback Prototypes: */

int  CVICALLBACK cancelCallback(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK funGenCallback(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK mainTab(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK measPanel(int panel, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ppaCallback(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK recoveryStart(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK setupBtnCallback(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK setupPanel(int panel, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK siggenCallback(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK spikeStart(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif
