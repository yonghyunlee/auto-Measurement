#include <formatio.h>
#include <utility.h>
#include <visa.h>
#include <ansi_c.h>
#include <cvirte.h>		
#include <userint.h>
#include "pulseAmp_ui.h"

static int setuppanel, measpanel;
static int SpikeTabPanelHandle, RecTabPanelHandle;

static char instrDescriptor[VI_FIND_BUFLEN]={0,};  
static ViUInt32 numInstrs;  
static ViFindList findList;  
static ViSession defaultRM, instr, instrPPA, instrSigGen, instrFunGen;  
static ViStatus status;  

static ViUInt32 retCount;
static ViUInt32 writeCount;
static unsigned char buffer[100];
static char stringinput[512], textBox[512]="";
static char descriptionPPA[50]="GPIB::", descriptionSigGen[50]="GPIB::", descriptionFunGen[50]="GPIB::", strTemp[50]="";
										       
static double sigFreq, sigAmpl, sigAmplST, sigAmplEND, funPeriod, funDuty, funOffset, funAmpl, flatVal;
static float xstart, xincre_ch1, xincre_ch4;
static int dataBytes_ch1, dataBytes_ch4;

static int threadFunctionID;
static int CVICALLBACK recMeasThread (void *functionData);
static int CVICALLBACK spikeMeasThread (void *functionData);
static int cancel = 0;
static int tabNumber = 0;

static char	*z = NULL;
static struct tm	*tm;
static time_t	tt;		 

static char currentPath[MAX_PATHNAME_LEN];
static int PPAsetting(), SIGsetting(), FUNsetting(), equipmentOn(), equipmentOff();
int readRecGraph();
int readSpikeGraph();

unsigned char StringToChar (unsigned char *string)
{
	int i;
	unsigned char tmp, tmp2;
	int length;
	length = strlen(string);
	
	if(length == 1) {
		string[1] = string[0];
		string[0] = '0';
	}
	tmp = string[0]-48;
	tmp2 = string[1]-48;
	if(tmp > 9) {
		tmp = string[0] - 97 + 10;
	}
	tmp = tmp*16;
	
	if(tmp2 > 9) {
		tmp2 = string[1] - 97 + 10;
	}
	return tmp+tmp2;
}

int main (int argc, char *argv[])
{
	if (InitCVIRTE (0, argv, 0) == 0)
		return -1;	/* out of memory */
	if ((setuppanel = LoadPanel (0, "pulseAmp_ui.uir", SETUPPANEL)) < 0)
		return -1;
	
	status = viOpenDefaultRM(&defaultRM);  
   if (status != VI_SUCCESS)  
   {  
      printf("Could not open a session to the VISA Resource Manager!\n");  
      exit (EXIT_FAILURE);  
   }   

/* 1.1 equipment list & 1st ******** intrumentDescriptor */  
   
   status = viFindRsrc(defaultRM, "?*INSTR", &findList, &numInstrs, instrDescriptor);  

   if (status < VI_SUCCESS)  
   {  
	   goto ErrInFindInstruments;
   }  
  
   				printf("%d instruments, serial ports, and other resources found:\n\n",numInstrs);  ////
   				printf("VISA Resource : %s \n",instrDescriptor);  
   strcat(textBox,"VISA Resource : ");															   ////
   strcat(textBox,instrDescriptor);
   strcat(textBox,"\n");

/* 1.2 1st ******* instrument session open (instr) */  
   
   status = viOpen (defaultRM, instrDescriptor, VI_NULL, VI_NULL, &instr);
   if (status < VI_SUCCESS)  
   {
        printf ("Cannot open a session to the device.\n");
        goto ErrInOpenInstrument;
   }


/* 1.3  write to the 1st instrument session (instr) */  

   strcpy(stringinput,"*IDN?");
   status = viWrite (instr, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
   if (status < VI_SUCCESS)  
   {
      printf("Error writing to the device\n");
      goto ErrInReadWrite;
   }

/* 1.4  read from the 1st instrument session (instr) */  
   
   status = viRead (instr, buffer, 100, &retCount);
   if (status < VI_SUCCESS) 
   {
      printf("Error reading a response from the device\n");
      goto ErrInReadWrite;   
   }
   
								      printf("Model : %*s\n",retCount,buffer);  ////
   strcat(textBox,"Model : ");
   strcat(textBox,buffer);
   strcat(textBox,"\n");

   status=viClose(instr);

   InsertListItem (setuppanel, SETUPPANEL_GERNERATOR, 0, buffer, instrDescriptor);
   InsertListItem (setuppanel, SETUPPANEL_PPA, 0, buffer, instrDescriptor);
   InsertListItem (setuppanel, SETUPPANEL_FUNGEN, 0, buffer, instrDescriptor);
          
   while (--numInstrs)  

   {  

	   
/* 2.1 find next ******** intrumentDescriptor */  
	   
      status = viFindNext (findList, instrDescriptor);  /* find next desriptor */  
      if (status < VI_SUCCESS)   
      {     
         printf ("An error occurred finding the next resource.\nHit enter to continue.");  
         goto ErrInFindInstruments;   
      }   
								      printf("VISA Resource : %s \n",instrDescriptor);  ////

	  strcat(textBox,"VISA Resource : ");
      strcat(textBox,instrDescriptor);
      strcat(textBox,"\n");

/* 2.2 Instrument session open (instr) */  

	  status = viOpen (defaultRM, instrDescriptor, VI_NULL, VI_NULL, &instr);  
      if (status < VI_SUCCESS)  
      {  
          printf ("An error occurred opening a session to %s\n",instrDescriptor); 
		  goto ErrInOpenInstrument;
      }  
        
/* 2.3  write to the next instrument session (instr) */  

   	  strcpy(stringinput,"*IDN?");
      status = viWrite (instr, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
      if (status < VI_SUCCESS)  
      {
         printf("Error writing to the device\n");
         goto ErrInReadWrite;
      }

/* 2.4  read from the next instrument session (instr) */  
   
      status = viRead (instr, buffer, 100, &retCount);
      if (status < VI_SUCCESS) 
      {
         printf("Error reading a response from the device\n");
         goto ErrInReadWrite;   
      }
  
	   strcat(textBox,"Model : ");
	   strcat(textBox,buffer);
	   strcat(textBox,"\n");
      										printf("Model : %*s\n",retCount,buffer);	  ////

      status=viClose(instr);
	  
	  InsertListItem (setuppanel, SETUPPANEL_GERNERATOR, 0, buffer, instrDescriptor);
	  InsertListItem (setuppanel, SETUPPANEL_PPA, 0, buffer, instrDescriptor);
	  InsertListItem (setuppanel, SETUPPANEL_FUNGEN, 0, buffer, instrDescriptor);
	  
   }    /* end while */  
   
	DisplayPanel(setuppanel);
	//  SetCtrlVal(SETUP,SETUP_TEXTBOX,"Test");
	//  SetCtrlVal(SETUP,SETUP_ADDSIGGEN,10);
	//  GetCtrlVal(panelHandle,SETUP_ADDSIGGEN,&iTest);
	//       printf("sigGen Add : %d\n",iTest);	 ////

	RunUserInterface();
// DiscardPanel(panelHandle)
   
	goto ErrInOpenInstrument;

	ErrInReadWrite:
	   status = viClose(instr);
   
	ErrInOpenInstrument: 
	   status = viClose(findList); 
   
	ErrInFindInstruments:
	   status = viClose(defaultRM);
   
//   printf(textBox);
   
   printf("Closing Sessions\nHit enter to continue.");
   fflush(stdin);
//   getchar();  
  
   return status;
}

int PPAsetting() {
	printf("PPA setting:");
	float timeOffset;
	if (tabNumber == 1) timeOffset = funPeriod*funDuty*0.01*1000;
	else timeOffset = funPeriod*funDuty*0.01*1000;
	
	// graph xstart
	xstart = timeOffset - 2.5;
	
	if (tabNumber == 1) {
		//Set PPA time scale
		strcpy(stringinput,"TIM:SCAL 0.5E-06");
		        								printf("\"%s\"\n",stringinput);	 ////     
		status = viWrite (instrPPA, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
		if (status < VI_SUCCESS)  
		{
		  printf("Error writing to the device\n");
		  return -1;
		}
		
		//Set PPA time offset
		sprintf(strTemp,"%fE-06", timeOffset);
		strcpy(stringinput,"TIM:OFFS ");
		strcat(stringinput, strTemp);
		        								printf("\"%s\"\n",stringinput);	 ////     
		status = viWrite (instrPPA, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
		if (status < VI_SUCCESS)  
		{
		  printf("Error writing to the device\n");
		  return -1;
		}
	} else {
		//Set PPA time scale
		//(strTemp,"%fE-06", timeOffset/5);
		strcpy(stringinput,"TIM:SCAL 0.5E-06");
		//strcat(stringinput, strTemp);
		        								printf("\"%s\"\n",stringinput);	 ////     
		status = viWrite (instrPPA, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
		if (status < VI_SUCCESS)  
		{
		  printf("Error writing to the device\n");
		  return -1;
		}
		//Set PPA time offset
		//sprintf(strTemp,"%fE-06", timeOffset);
		strcpy(stringinput,"TIM:OFFS 2.5E-06");
		//strcat(stringinput, strTemp);
		        								printf("\"%s\"\n",stringinput);	 ////     
		status = viWrite (instrPPA, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
		if (status < VI_SUCCESS)  
		{
		  printf("Error writing to the device\n");
		  return -1;
		}
	}
		
	
	
	////set PPA Timeout					      
	//status =  viSetAttribute(instrPPA, VI_ATTR_TMO_VALUE, 15000);
	//if (status < VI_SUCCESS)  
	//{
	//  printf("Error writing to the device\n");
	//  return -1;
	//}
	
	return 0;
}

int FUNsetting() {
	printf("FUN setting:");
	//Set Function Generator Pulse mode
	strcpy(stringinput,":FUNCtion PULSe");
	        								printf("\"%s\"\n",stringinput);	 ////     
	status = viWrite (instrFunGen, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
	if (status < VI_SUCCESS)  
	{
	  printf("Error writing to the device\n");
	  return -1;
	}
	
	
	//Set Function Generator Amplitude
	sprintf(strTemp,"%d", (int)funAmpl);
	strcpy(stringinput,":VOLTage ");
	strcat(stringinput, strTemp);
	        								printf("\"%s\"\n",stringinput);	 ////     
	status = viWrite (instrFunGen, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
	if (status < VI_SUCCESS)  
	{
	  printf("Error writing to the device\n");
	  return -1;
	}
	
	//Set Function Generator Offset
	sprintf(strTemp,"%d", (int)funOffset);
	strcpy(stringinput,":VOLTage:OFFSet ");
	strcat(stringinput, strTemp);
	        								printf("\"%s\"\n",stringinput);	 ////     
	status = viWrite (instrFunGen, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
	if (status < VI_SUCCESS)  
	{
	  printf("Error writing to the device\n");
	  return -1;
	}
	
	//Set Function Generator Period
	sprintf(strTemp,"%fe-3", funPeriod);
	strcpy(stringinput,":PULSe:PERiod ");
	strcat(stringinput, strTemp);
	        								printf("\"%s\"\n",stringinput);	 ////     
	status = viWrite (instrFunGen, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
	if (status < VI_SUCCESS)  
	{
	  printf("Error writing to the device\n");
	  return -1;
	}
	
	//Set Function Generator Duty Cycle
	sprintf(strTemp,"%d", (int) funDuty);
	strcpy(stringinput,"FUNCtion:PULSe:DCYCle ");
	strcat(stringinput, strTemp);
	        								printf("\"%s\"\n",stringinput);	 ////     
	status = viWrite (instrFunGen, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
	if (status < VI_SUCCESS)  
	{
	  printf("Error writing to the device\n");
	  return -1;
	}
	
	return 0;
}

int SIGsetting() {
	printf("SIG setting:");
	/* Signal Generator Frequency setting */
	sprintf(strTemp,"%d", (int) (sigFreq*1000));
	strcpy(stringinput,":FREQ ");
	strcat(stringinput, strTemp);
	strcat(stringinput,"MHz");
	        								printf("\"%s\"\n",stringinput);	 ////     
	status = viWrite (instrSigGen, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
	if (status < VI_SUCCESS)  
	{
	  printf("Error writing to the device\n");
	  return -1;
	}

	/* Signal Generator Power setting */
	if(sigAmpl > 15) return 1;
	sprintf(strTemp,"%f", sigAmpl);
	strcpy(stringinput,":POW ");
   	strcat(stringinput,strTemp);
   	strcat(stringinput,"dBm");
	        								printf("\"%s\"\n",stringinput);	 ////     
	status = viWrite (instrSigGen, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
	if (status < VI_SUCCESS)  
	{
	  printf("Error writing to the device\n");
	  return -1;
	}

	return 0; 
}

int equipmentOn() {
	 /* Signal Generator Power ON */
	strcpy(stringinput,":OUTP ON\n");
	        								printf("\"%s\"\n",stringinput);	 ////     
	status = viWrite (instrSigGen, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
	if (status < VI_SUCCESS)  
	{
	  printf("Error writing to the device\n");
	  return -1;
	}
	
	//Function Generator On
	strcpy(stringinput,":OUTP ON\n");
	        								printf("\"%s\"\n",stringinput);	 ////     
	status = viWrite (instrFunGen, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
	if (status < VI_SUCCESS)  
	{
	  printf("Error writing to the device\n");
	  return -1;
	}
	return 0;
}

int equipmentOff() {
	 /* Signal Generator Power ON */
	strcpy(stringinput,":OUTP OFF\n");
	        								printf("\"%s\"\n",stringinput);	 ////     
	status = viWrite (instrSigGen, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
	if (status < VI_SUCCESS)  
	{
	  printf("Error writing to the device\n");
	  return -1;
	}
	
	//Function Generator On
	strcpy(stringinput,":OUTP OFF\n");
	        								printf("\"%s\"\n",stringinput);	 ////     
	status = viWrite (instrFunGen, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
	if (status < VI_SUCCESS)  
	{
	  printf("Error writing to the device\n");
	  return -1;
	}
	return 0;
}

int CVICALLBACK measPanel (int panel, int event, void *callbackData,
						   int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_GOT_FOCUS:

			break;
		case EVENT_LOST_FOCUS:

			break;
		case EVENT_CLOSE:
			QuitUserInterface(0);
			break;
	}
	return 0;
}

int CVICALLBACK setupPanel (int panel, int event, void *callbackData,
							int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_GOT_FOCUS:

			break;
		case EVENT_LOST_FOCUS:

			break;
		case EVENT_CLOSE:
			QuitUserInterface(0);
			break;
	}
	return 0;
}



int CVICALLBACK cancelCallback (int panel, int control, int event,
								void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:
			if (cancel == 0) {
				cancel = 1;
			}
			SetCtrlAttribute (panel, control, ATTR_DIMMED, 1);
			equipmentOff();
			break;
	}
	return 0;
}

int CVICALLBACK setupBtnCallback (int panel, int control, int event,
								  void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:
			GetCtrlVal(panel,SETUPPANEL_SELECTEDPPA, descriptionPPA);		///////////////////////????????????????????????????????
			GetCtrlVal(panel,SETUPPANEL_SELETEDSIGTEXT, descriptionSigGen);
			GetCtrlVal(panel,SETUPPANEL_FUNGENTEXT, descriptionFunGen);///////////////////////???????????????????????????????? panelHandle -> SETUP
			
			printf("Signal Generator : %s\n", descriptionSigGen);
			printf("Peak Power Analyzer : %s\n\n", descriptionPPA);
			printf("Function Generator : %s\n\n", descriptionFunGen);
			DiscardPanel(panel);
			
			if ((panel=LoadPanel(0,"pulseAmp_ui.uir", MEAS))<0)
				return -1;
			
			measpanel = panel;
			DisplayPanel(panel);
			
			GetPanelHandleFromTabPage (measpanel, MEAS_TAB, 0, &SpikeTabPanelHandle);
			GetPanelHandleFromTabPage (measpanel, MEAS_TAB, 1, &RecTabPanelHandle);
			
			
			
			GetDir(currentPath);
			
			break;
	}
	return 0;
}

int CVICALLBACK siggenCallback (int panel, int control, int event,
								void *callbackData, int eventData1, int eventData2)
{
	char instr[VI_FIND_BUFLEN];
	switch (event)
	{
		case EVENT_COMMIT:
			GetCtrlVal (panel, control, instr);
			SetCtrlVal(panel, SETUPPANEL_SELETEDSIGTEXT, instr);
			break;
	}
	return 0;
}

int CVICALLBACK ppaCallback (int panel, int control, int event,
							 void *callbackData, int eventData1, int eventData2)
{
	char instr[VI_FIND_BUFLEN];
	switch (event)
	{
		case EVENT_COMMIT:
			GetCtrlVal (panel, control, instr);
			SetCtrlVal(panel, SETUPPANEL_SELECTEDPPA, instr);
			break;
	}
	return 0;
}


static int CVICALLBACK recMeasThread (void *functionData){
	int i =0;
	cancel = 0;
	
	char *key, *val;
	
	FILE* saveFile;
	unsigned char msg[100];
	int inc;
	char fileName[MAX_PATHNAME_LEN];
	char dirName[MAX_PATHNAME_LEN];
	char projectDir[MAX_PATHNAME_LEN];
	
	// setting parsing
	GetCtrlVal(RecTabPanelHandle, REC_TAB_SIG_FREQ, &sigFreq);
	GetCtrlVal(RecTabPanelHandle, REC_TAB_SIG_DB_ST, &sigAmplST);
	GetCtrlVal(RecTabPanelHandle, REC_TAB_SIG_DB_END, &sigAmplEND);
	GetCtrlVal(RecTabPanelHandle, REC_TAB_FUN_PERIOD,&funPeriod);
	GetCtrlVal(RecTabPanelHandle, REC_TAB_FUN_AMPL,&funAmpl);
	GetCtrlVal(RecTabPanelHandle, REC_TAB_FUN_OFFSET,&funOffset);
	GetCtrlVal(RecTabPanelHandle, REC_TAB_FUN_DUTY,&funDuty);
	GetCtrlVal(RecTabPanelHandle, REC_TAB_REC_INPUT,&flatVal);
	
	
	status = viOpen (defaultRM, descriptionPPA, VI_NULL, VI_NULL, &instrPPA);
	if (status < VI_SUCCESS)  
	{
	  printf ("Cannot open a session to the device.\n");
	  return -1;
	}
	
	status = viOpen (defaultRM, descriptionSigGen, VI_NULL, VI_NULL, &instrSigGen);
	if (status < VI_SUCCESS)  
	{
	  printf ("Cannot open a session to the device.\n");
	  return -1;
	}
	
	status = viOpen (defaultRM, descriptionFunGen, VI_NULL, VI_NULL, &instrFunGen);
	if (status < VI_SUCCESS)  
	{
	  printf ("Cannot open a session to the device.\n");
	  return -1;
	}
	
	// setting
	if(PPAsetting()) return -1;
	if(FUNsetting()) return -1;
	
	sigAmpl = sigAmplST;
	
	if(SIGsetting()) return -1;
	
	equipmentOn();
	
	i=0;
	sprintf(dirName, "%.2fms,%.2f%%", funPeriod, funDuty);
	if (GetProjectDir (projectDir) < 0)
		FmtOut ("Project is untitled\n");
	else {
		MakePathname (projectDir, dirName, dirName); 
		int errors = SetBreakOnLibraryErrors(0);
		MakeDir (dirName);
		SetBreakOnLibraryErrors(errors);
		/*while (MakeDir (dirName) != -9) {
			i++;
			char indexDir[5];
			sprintf(indexDir, " (%d)", i);
			strcat(dirName, indexDir);
		}*/
	}
	
	while(sigAmpl <= sigAmplEND) {
		Delay(2);
		if (cancel == 1) return 0;
		//data ch1
		strcpy(stringinput,"TRACe:DATA? CHAN1\n");
		        								printf("\"%s\"\n",stringinput);	 ////    
		status = viWrite (instrPPA, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
		if (status < VI_SUCCESS)  
		{
		  printf("Error writing to the device\n");
		  return -1;
		}

		status = viRead (instrPPA, buffer, 2, &retCount);
		if (status < VI_SUCCESS) 
		{
		   printf("Error reading a response from the device\n");
		   return -1;
		}
		buffer[2] = '\0';
		printf("data: %s\n", buffer);
		inc = atoi(&buffer[1]);
		printf("inc: %d\n", inc);
		buffer[inc] = '\0';
		status = viRead (instrPPA, buffer, inc, &retCount);
		if (status < VI_SUCCESS) 
		{
		   printf("Error reading a response from the device\n");
		   return -1;
		}
	
		dataBytes_ch1 = atoi(buffer);
		printf("ch1 bytes: %d\n", dataBytes_ch1);
	
		sprintf(fileName, "./%.2fms,%.2f%%/", funPeriod, funDuty);
		strcat(fileName, "recovery_ch1.txt");
		saveFile = OpenFile (fileName, VAL_WRITE_ONLY, VAL_TRUNCATE, VAL_ASCII);
	
		for(i=0; i<dataBytes_ch1; i++) {
			status = viRead (instrPPA, buffer, 1, &retCount);
			if (status < VI_SUCCESS) 
			{
			   printf("Error reading a response from the device\n");
			   return -1;
			}
			//printf("%x\n", buffer[0]);
			sprintf (msg, "%x", buffer[0]);
			WriteLine (saveFile, msg, -1);
		}
	
		//MessagePopup("ch1 success", "success");
	
		CloseFile (saveFile);
	
		if (cancel == 1) return 0;
		
		//data ch4
		strcpy(stringinput,"TRACe:DATA? CHAN4\n");
		        								printf("\"%s\"\n",stringinput);	 ////    
		status = viWrite (instrPPA, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
		if (status < VI_SUCCESS)  
		{
		  printf("Error writing to the device\n");
		  return -1;
		}

		status = viRead (instrPPA, buffer, 2, &retCount);
		if (status < VI_SUCCESS) 
		{
		   printf("Error reading a response from the device\n");
		   return -1;
		}
		buffer[2] = '\0';
		printf("data: %s\n", buffer);
		inc = atoi(&buffer[1]);
		printf("inc: %d\n", inc);
		buffer[inc] = '\0';
		status = viRead (instrPPA, buffer, inc, &retCount);
		if (status < VI_SUCCESS) 
		{
		   printf("Error reading a response from the device\n");
		   return -1;
		}
	
	
		dataBytes_ch4 = atoi(buffer);
		printf("bytes: %d\n", dataBytes_ch4);
	
		sprintf(fileName, "./%.2fms,%.2f%%/", funPeriod, funDuty);
		strcat(fileName, "recovery_ch4.txt");
		saveFile = OpenFile (fileName, VAL_WRITE_ONLY, VAL_TRUNCATE, VAL_ASCII);
	
		for(i=0; i<dataBytes_ch4; i++) {
			status = viRead (instrPPA, buffer, 1, &retCount);
			if (status < VI_SUCCESS) 
			{
			   printf("Error reading a response from the device\n");
			   return -1;
			}
			sprintf (msg, "%x", buffer[0]);
			WriteLine (saveFile, msg, -1);
		}
	
		CloseFile (saveFile);
		if (cancel == 1) return 0;
		readRecGraph();
		
		sigAmpl++;
		if(SIGsetting()) return -1;
	}
	
	equipmentOff();
	 
	return 0;
}

static int CVICALLBACK spikeMeasThread (void *functionData){
	int i =0;
	cancel = 0;
	
	char *key, *val;
	
	FILE* saveFile;
	unsigned char msg[100];
	int inc;
	char fileName[MAX_PATHNAME_LEN];
	char dirName[MAX_PATHNAME_LEN];
	char projectDir[MAX_PATHNAME_LEN];
	
	GetCtrlVal(SpikeTabPanelHandle, SPIKE_TAB_SIG_FREQ, &sigFreq);
	GetCtrlVal(SpikeTabPanelHandle, SPIKE_TAB_SIG_DB_ST, &sigAmplST);
	GetCtrlVal(SpikeTabPanelHandle, SPIKE_TAB_SIG_DB_END, &sigAmplEND);
	GetCtrlVal(SpikeTabPanelHandle, SPIKE_TAB_FUN_PERIOD, &funPeriod);
	GetCtrlVal(SpikeTabPanelHandle, SPIKE_TAB_FUN_AMPL, &funAmpl);
	GetCtrlVal(SpikeTabPanelHandle, SPIKE_TAB_FUN_OFFSET, &funOffset);
	GetCtrlVal(SpikeTabPanelHandle, SPIKE_TAB_FUN_DUTY, &funDuty);
	
	status = viOpen (defaultRM, descriptionPPA, VI_NULL, VI_NULL, &instrPPA);
	if (status < VI_SUCCESS)  
	{
	  printf ("Cannot open a session to the device.\n");
	  return -1;
	}
	
	status = viOpen (defaultRM, descriptionSigGen, VI_NULL, VI_NULL, &instrSigGen);
	if (status < VI_SUCCESS)  
	{
	  printf ("Cannot open a session to the device.\n");
	  return -1;
	}
	
	status = viOpen (defaultRM, descriptionFunGen, VI_NULL, VI_NULL, &instrFunGen);
	if (status < VI_SUCCESS)  
	{
	  printf ("Cannot open a session to the device.\n");
	  return -1;
	}
	
	// setting
	if(PPAsetting()) return -1;
	if(FUNsetting()) return -1;
	
	sigAmpl = sigAmplST;
	
	if(SIGsetting()) return -1;
	
	equipmentOn();
	
	sprintf(dirName, "%.2fms,%.2f%%", funPeriod, funDuty);
	if (GetProjectDir (projectDir) < 0)
		FmtOut ("Project is untitled\n");
	else {
		MakePathname (projectDir, dirName, dirName); 
		int errors = SetBreakOnLibraryErrors(0);
		MakeDir (dirName);
		SetBreakOnLibraryErrors(errors);
	}
	
	while(sigAmpl <= sigAmplEND) {
		Delay(2);
		if (cancel == 1) return 0;
		//data ch1
		strcpy(stringinput,"TRACe:DATA? CHAN1\n");
		        								printf("\"%s\"\n",stringinput);	 ////    
		status = viWrite (instrPPA, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
		if (status < VI_SUCCESS)  
		{
		  printf("Error writing to the device\n");
		  return -1;
		}

		status = viRead (instrPPA, buffer, 2, &retCount);
		if (status < VI_SUCCESS) 
		{
		   printf("Error reading a response from the device\n");
		   return -1;
		}
		buffer[2] = '\0';
		printf("data: %s\n", buffer);
		inc = atoi(&buffer[1]);
		printf("inc: %d\n", inc);
		buffer[inc] = '\0';
		status = viRead (instrPPA, buffer, inc, &retCount);
		if (status < VI_SUCCESS) 
		{
		   printf("Error reading a response from the device\n");
		   return -1;
		}
	
		dataBytes_ch1 = atoi(buffer);
		printf("ch1 bytes: %d\n", dataBytes_ch1);
	
		sprintf(fileName, "./%.2fms,%.2f%%/", funPeriod, funDuty);
		strcat(fileName, "spike_ch1.txt");
		saveFile = OpenFile (fileName, VAL_WRITE_ONLY, VAL_OPEN_AS_IS, VAL_ASCII);
	
		for(i=0; i<dataBytes_ch1; i++) {
			status = viRead (instrPPA, buffer, 1, &retCount);
			if (status < VI_SUCCESS) 
			{
			   printf("Error reading a response from the device\n");
			   return -1;
			}
			//printf("%x\n", buffer[0]);
			sprintf (msg, "%x", buffer[0]);
			WriteLine (saveFile, msg, -1);
		}
	
		CloseFile (saveFile);
		
		if (cancel == 1) return 0;
		
		//data ch4
		strcpy(stringinput,"TRACe:DATA? CHAN4\n");
		        								printf("\"%s\"\n",stringinput);	 ////    
		status = viWrite (instrPPA, (ViBuf)stringinput, (ViUInt32)strlen(stringinput), &writeCount);
		if (status < VI_SUCCESS)  
		{
		  printf("Error writing to the device\n");
		  return -1;
		}

		status = viRead (instrPPA, buffer, 2, &retCount);
		if (status < VI_SUCCESS) 
		{
		   printf("Error reading a response from the device\n");
		   return -1;
		}
		buffer[2] = '\0';
		printf("data: %s\n", buffer);
		inc = atoi(&buffer[1]);
		printf("inc: %d\n", inc);
		buffer[inc] = '\0';
		status = viRead (instrPPA, buffer, inc, &retCount);
		if (status < VI_SUCCESS) 
		{
		   printf("Error reading a response from the device\n");
		   return -1;
		}
	
	
		dataBytes_ch4 = atoi(buffer);
		printf("bytes: %d\n", dataBytes_ch4);
	
		sprintf(fileName, "./%.2fms,%.2f%%/", funPeriod, funDuty);
		strcat(fileName, "spike_ch4.txt");
		saveFile = OpenFile (fileName, VAL_WRITE_ONLY, VAL_OPEN_AS_IS, VAL_ASCII);
	
		for(i=0; i<dataBytes_ch4; i++) {
			status = viRead (instrPPA, buffer, 1, &retCount);
			if (status < VI_SUCCESS) 
			{
			   printf("Error reading a response from the device\n");
			   return -1;
			}
			sprintf (msg, "%x", buffer[0]);
			WriteLine (saveFile, msg, -1);
		}
	
		CloseFile (saveFile);
		
		if (cancel == 1) return 0;
		
		readSpikeGraph();
		
		sigAmpl++;
		if(SIGsetting()) return -1;
	}
	
	equipmentOff();
	
	return 0;
}

int CVICALLBACK funGenCallback (int panel, int control, int event,
								void *callbackData, int eventData1, int eventData2)
{
	char instr[VI_FIND_BUFLEN];
	switch (event)
	{
		case EVENT_COMMIT:
			GetCtrlVal (panel, control, instr);
			SetCtrlVal(panel, SETUPPANEL_FUNGENTEXT, instr);
			break;
	}
	return 0;
}

int CVICALLBACK ampListCallback (int panel, int control, int event,
								 void *callbackData, int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_COMMIT:

			break;
	}
	return 0;
}

int CVICALLBACK measReadCallback (int panel, int control, int event,
								  void *callbackData, int eventData1, int eventData2)
{
	
	return 0;
}

int CVICALLBACK mainTab (int panel, int control, int event,
						 void *callbackData, int eventData1, int eventData2)
{
	return 0;
}

int CVICALLBACK spikeStart (int panel, int control, int event,
							void *callbackData, int eventData1, int eventData2)
{
	char fileName[MAX_PATHNAME_LEN];
	FILE* saveDataFile;
	unsigned char msg[100];
	switch (event)
	{
		case EVENT_COMMIT:
			cancel = 0;
			tabNumber = 0;
			
			GetCtrlVal(SpikeTabPanelHandle, SPIKE_TAB_SAVE_FILENAME, fileName);
			saveDataFile = OpenFile (fileName, VAL_WRITE_ONLY, VAL_APPEND, VAL_ASCII);
			sprintf (msg, "--------------------------------------");
			WriteLine (saveDataFile, msg, -1);
			CloseFile(saveDataFile);
			
			CmtScheduleThreadPoolFunction (DEFAULT_THREAD_POOL_HANDLE, spikeMeasThread, NULL, &threadFunctionID);//schedule the thread passing our functions
			SetCtrlAttribute (panel, SPIKE_TAB_CANCEL, ATTR_DIMMED, 0);
			CmtWaitForThreadPoolFunctionCompletion(DEFAULT_THREAD_POOL_HANDLE, threadFunctionID, OPT_TP_PROCESS_EVENTS_WHILE_WAITING); //wait for the thread to complete
			CmtReleaseThreadPoolFunctionID(DEFAULT_THREAD_POOL_HANDLE, threadFunctionID); //release thread resources.
			
			
			viClose(instrPPA);
			viClose(instrFunGen);
			viClose(instrSigGen);
			SetCtrlAttribute (panel, SPIKE_TAB_CANCEL, ATTR_DIMMED, 1);
			break;
	}
	return 0;
}

int CVICALLBACK recoveryStart (int panel, int control, int event,
							   void *callbackData, int eventData1, int eventData2)
{
	char fileName[MAX_PATHNAME_LEN];
	FILE* saveDataFile;
	unsigned char msg[100];
	switch (event)
	{
		case EVENT_COMMIT:
			cancel = 0;
			tabNumber = 1;
			
			// line
			GetCtrlVal(RecTabPanelHandle, REC_TAB_SAVE_FILENAME, fileName);
			saveDataFile = OpenFile (fileName, VAL_WRITE_ONLY, VAL_APPEND, VAL_ASCII);
			sprintf (msg, "--------------------------------------");
			WriteLine (saveDataFile, msg, -1);
			CloseFile(saveDataFile);
			
			CmtScheduleThreadPoolFunction (DEFAULT_THREAD_POOL_HANDLE, recMeasThread, NULL, &threadFunctionID);//schedule the thread passing our functions
			SetCtrlAttribute (panel, REC_TAB_CANCEL, ATTR_DIMMED, 0);
			CmtWaitForThreadPoolFunctionCompletion(DEFAULT_THREAD_POOL_HANDLE, threadFunctionID, OPT_TP_PROCESS_EVENTS_WHILE_WAITING); //wait for the thread to complete
			CmtReleaseThreadPoolFunctionID(DEFAULT_THREAD_POOL_HANDLE, threadFunctionID); //release thread resources.
			
			
			viClose(instrPPA);
			viClose(instrFunGen);
			viClose(instrSigGen);
			SetCtrlAttribute (panel, SPIKE_TAB_CANCEL, ATTR_DIMMED, 1);
			break;
	}
	return 0;
}

int readRecGraph() {
	//printf("data : %d, %d", dataBytes_ch1/4, dataBytes_ch4/4);
	FILE* logFile;
	FILE* saveDataFile;
	int error;
	int count = 0;
	unsigned char buf[10];
	unsigned char bufc[4];
	unsigned char msg[100];
	int length;
	int i;
	float re_start_x, re_end_x, re_end_y;
	float ch1_xdata[dataBytes_ch1/4], ch1_ydata[dataBytes_ch1/4];
	float ch4_xdata[dataBytes_ch4/4], ch4_ydata[dataBytes_ch4/4];
	
	char fileName[MAX_PATHNAME_LEN];
	
	char inciChar[10];
	char oriFileName[MAX_PATHNAME_LEN];
	char newFileName[MAX_PATHNAME_LEN];
	char projectDir[MAX_PATHNAME_LEN];
	char oriFullPath[MAX_PATHNAME_LEN];
	char newFullPath[MAX_PATHNAME_LEN];
	char pulseSetting[20];
	
	GetCtrlVal(RecTabPanelHandle, REC_TAB_SAVE_FILENAME, fileName);
	
	saveDataFile = OpenFile (fileName, VAL_WRITE_ONLY, VAL_APPEND, VAL_ASCII);
	sprintf(pulseSetting, "%.2fms,%.2f%%\\", funPeriod, funDuty);
	
	// xinc
	xincre_ch1 = 0.01;
	printf("xincre : %f\n", xincre_ch1);
	xincre_ch4 = 0.01;
	
	//ch1 graph
	i = 0;
	sprintf(fileName, "./%.2fms,%.2f%%/", funPeriod, funDuty);
	strcat(fileName, "recovery_ch1.txt");
	logFile = OpenFile (fileName, VAL_READ_ONLY, VAL_OPEN_AS_IS, VAL_ASCII);
	
	while ((error = ReadLine (logFile, buf, -1)) != -2) {
	    if (error == -1) {
	        error = GetFmtIOError ();
			MessagePopup("Error", GetFmtIOErrorString (error));
	    }
		bufc[3-count] = StringToChar(buf);
		
		count++;
		if (count == 4) {
			ch1_ydata[i] = *(float *) bufc;
			i++;
			count = 0;
		}
		
	}
	
	for (i=0; i<dataBytes_ch1/4; i++){
		ch1_xdata[i] = xstart + i*xincre_ch1;
	}
	
	PlotXY (RecTabPanelHandle, REC_TAB_GRAPH, ch1_xdata, ch1_ydata, dataBytes_ch1/4, VAL_FLOAT, VAL_FLOAT,
		VAL_THIN_LINE, VAL_NO_POINT, VAL_SOLID, 1, VAL_GREEN);
	
	float middle = ch1_ydata[dataBytes_ch1/4/2];
	printf("middle : %f\n", middle);
	/*for (i=0; i<dataBytes_ch1/4; i++){
		if(ch1_ydata[i] < middle) break;
	}*/
	
	re_start_x = ch1_xdata[dataBytes_ch1/4/2];
	printf("x1 : %f\n", re_start_x);
	
	//incident
	float sum=0, avg;
	for(i=0; i<(dataBytes_ch1/4/2); i++) {
		sum += ch1_ydata[i];
	}
	avg = sum/((dataBytes_ch1/4)/2);
	SetCtrlVal (RecTabPanelHandle, REC_TAB_INCIDENT, avg);
	
	// write settings
	sprintf (msg, "*incident\t%f", avg);
	WriteLine (saveDataFile, msg, -1);
	
	CloseFile(logFile);
	
	// ch1 rename graph file
	sprintf(inciChar, "%f", avg);
	strcpy(oriFileName, pulseSetting);
	strcat(oriFileName,"recovery_ch1.txt");
	strcpy(newFileName, pulseSetting);
	strcat(newFileName,"recovery_ch1_");
	strcat(newFileName, inciChar);
	strcat(newFileName, ".txt");
	
	if (GetProjectDir (projectDir) < 0)
		FmtOut ("Project is untitled\n");
	else {
		MakePathname (projectDir, oriFileName, oriFullPath);// where fullPath has the following format: c:\myproject\myfile.dat
		MakePathname (projectDir, newFileName, newFullPath);
		printf("%s %s\n", oriFullPath, newFullPath);
	}
	RenameFile(oriFullPath, newFullPath);
	
	
	
	// ch4 graph
	i=0;
	sprintf(fileName, "./%.2fms,%.2f%%/", funPeriod, funDuty);
	strcat(fileName, "recovery_ch4.txt");
	logFile = OpenFile (fileName, VAL_READ_ONLY, VAL_OPEN_AS_IS, VAL_ASCII);
	
	while ((error = ReadLine (logFile, buf, -1)) != -2) {
	    if (error == -1) {
	        error = GetFmtIOError ();
			MessagePopup("Error", GetFmtIOErrorString (error));
	    }
		bufc[3-count] = StringToChar(buf);
		
		count++;
		if (count == 4) {
			ch4_ydata[i] = *(float *) bufc;
			i++;
			count = 0;
		}
	}
	printf("%d\n", i);
	
	for (i=0; i<dataBytes_ch4/4; i++){
		ch4_xdata[i] = xstart + i*xincre_ch4;
	}
	
	for (i=0; i<dataBytes_ch4/4; i++){
		if ((ch4_xdata[i] - re_start_x) < 0.01 && (ch4_xdata[i] - re_start_x) > -0.01) break;
	}
	
	printf("%f. %f\n", ch4_xdata[i], ch4_ydata[i]);
	
	PlotXY (RecTabPanelHandle, REC_TAB_GRAPH, ch4_xdata, ch4_ydata, dataBytes_ch4/4, VAL_FLOAT, VAL_FLOAT,
		VAL_THIN_LINE, VAL_NO_POINT, VAL_SOLID, 1, VAL_MAGENTA);
	
	PlotXY (RecTabPanelHandle, REC_TAB_GRAPH, &ch4_xdata[i], &ch4_ydata[i], 1, VAL_FLOAT, VAL_FLOAT,
		VAL_SCATTER, VAL_BOLD_X, VAL_SOLID, 1, VAL_RED);
	
	printf("%f\n", flatVal);
	
	re_end_y = flatVal-3;
	printf("%f\n", re_end_y);
	
	for (i=0; i<dataBytes_ch4/4; i++){
		if ((ch4_ydata[i]) > re_end_y) break;
	}
	
	if((dataBytes_ch4/4) <= i) {
		cancel = 1;
		SetCtrlAttribute (RecTabPanelHandle, REC_TAB_CANCEL, ATTR_DIMMED, 1);
		equipmentOff();
		CloseFile(logFile);
		CloseFile(saveDataFile);
		return 0;
	}
	
	re_end_x = ch4_xdata[i];
	
	PlotXY (RecTabPanelHandle, REC_TAB_GRAPH, &ch4_xdata[i], &ch4_ydata[i], 1, VAL_FLOAT, VAL_FLOAT,
		VAL_SCATTER, VAL_BOLD_X, VAL_SOLID, 1, VAL_RED);
	
	printf("end: %f - start: %f = recovery: %f\n", re_end_x, re_start_x, re_end_x - re_start_x);
	SetCtrlVal (RecTabPanelHandle, REC_TAB_RECOVERY, (re_end_x - re_start_x));
	CloseFile(logFile);
	
	// ch4 rename graph file
	sprintf(inciChar, "%f", avg);
	strcpy(oriFileName, pulseSetting);
	strcat(oriFileName,"recovery_ch4.txt");
	strcpy(newFileName, pulseSetting);
	strcat(newFileName,"recovery_ch4_");
	strcat(newFileName, inciChar);
	strcat(newFileName, ".txt");
	
	MakePathname (projectDir, oriFileName, oriFullPath);// where fullPath has the following format: c:\myproject\myfile.dat
	MakePathname (projectDir, newFileName, newFullPath);
	RenameFile(oriFullPath, newFullPath);
	
	// write recovery time
	sprintf (msg, "*recovery time\t%f", (re_end_x - re_start_x));
	WriteLine (saveDataFile, msg, -1);

	CloseFile(saveDataFile);
	
	return 0;
}

int readSpikeGraph() {
	 FILE* logFile;
	 FILE* saveDataFile;
	int error;
	int count = 0;
	float ch1_xdata[dataBytes_ch1/4], ch1_ydata[dataBytes_ch1/4];
	float ch4_xdata[dataBytes_ch4/4], ch4_ydata[dataBytes_ch4/4];
	unsigned char buf[10];
	unsigned char bufc[4];
	unsigned char msg[100];
	int i;
	float ch4_end_y;
	float sp_start_x, sp_start_y, sp_end_x, sp_end_y, sp_start_index, sp_end_index;
	float incident;
	
	char inciChar[10];
	char oriFileName[MAX_PATHNAME_LEN];
	char newFileName[MAX_PATHNAME_LEN];
	char projectDir[MAX_PATHNAME_LEN];
	char oriFullPath[MAX_PATHNAME_LEN];
	char newFullPath[MAX_PATHNAME_LEN];
	char pulseSetting[20];
	char fileName[MAX_PATHNAME_LEN];
	float max;
	int maxIndex;
	
	GetCtrlVal(SpikeTabPanelHandle, SPIKE_TAB_SAVE_FILENAME, fileName);
	saveDataFile = OpenFile (fileName, VAL_WRITE_ONLY, VAL_APPEND, VAL_ASCII);
	sprintf(pulseSetting, "%.2fms,%.2f%%\\", funPeriod, funDuty);
	// write settings
	sprintf (msg, "*sig Ampl: %f", sigAmpl);
	WriteLine (saveDataFile, msg, -1);
	
	// ch1 graph
	i = 0;
	sprintf(fileName, "./%.2fms,%.2f%%/", funPeriod, funDuty);
	strcat(fileName, "spike_ch1.txt");
	logFile = OpenFile (fileName, VAL_READ_ONLY, VAL_OPEN_AS_IS, VAL_ASCII);
	
	while ((error = ReadLine (logFile, buf, -1)) != -2) {
	    if (error == -1) {
	        error = GetFmtIOError ();
			MessagePopup("Error", GetFmtIOErrorString (error));
			break;
	    }
		bufc[3-count] = StringToChar(buf);
		
		count++;
		if (count == 4) {
			ch1_ydata[i] = *(float *) bufc;
			i++;
			count = 0;
		}
		
	}
	
	float start = xstart;
	for (i=0; i<dataBytes_ch1/4; i++){
		ch1_xdata[i] = start + i*0.01;
	}
	
	PlotXY (SpikeTabPanelHandle, SPIKE_TAB_GRAPH, ch1_xdata, ch1_ydata, dataBytes_ch1/4, VAL_FLOAT, VAL_FLOAT,
		VAL_THIN_LINE, VAL_NO_POINT, VAL_SOLID, 1, VAL_GREEN);

	
	
	CloseFile(logFile);
	
	// ch4 graph
	i=0;
	sprintf(fileName, "./%.2fms,%.2f%%/", funPeriod, funDuty);
	strcat(fileName, "spike_ch4.txt");
	logFile = OpenFile (fileName, VAL_READ_ONLY, VAL_OPEN_AS_IS, VAL_ASCII);
	
	while ((error = ReadLine (logFile, buf, -1)) != -2) {
	    if (error == -1) {
	        error = GetFmtIOError ();
			MessagePopup("Error", GetFmtIOErrorString (error));
	    }
		bufc[3-count] = StringToChar(buf);
		
		count++;
		if (count == 4) {
			ch4_ydata[i] = *(float *) bufc;
			i++;
			count = 0;
		}
	}
	
	
	start = xstart;
	for (i=0; i<dataBytes_ch4/4; i++){
		ch4_xdata[i] = start + i*0.01;
	}
	
	PlotXY (SpikeTabPanelHandle, SPIKE_TAB_GRAPH, ch4_xdata, ch4_ydata, dataBytes_ch4/4, VAL_FLOAT, VAL_FLOAT,
		VAL_THIN_LINE, VAL_NO_POINT, VAL_SOLID, 1, VAL_MAGENTA);
	
	
	
	CloseFile(logFile);
	
	// end 3 points average = flat leakage
	ch4_end_y = (ch4_ydata[dataBytes_ch4/4-1]+ch4_ydata[dataBytes_ch4/4-2]+ch4_ydata[dataBytes_ch4/4-3])/3;
	printf("3 end average: %f\n", ch4_end_y);
	
	// spike start, end, pulse width
	for (i=0; i<dataBytes_ch4/4; i++){
		if ((ch4_ydata[i]) > ch4_end_y) break;
	}
	
	sp_start_x = ch4_xdata[i];
	sp_start_y = ch4_ydata[i];
	sp_start_index = i;
	
	for (; i<dataBytes_ch4/4; i++){
		if ((ch4_ydata[i]) <= ch4_end_y) break;
	}
	
	sp_end_x = ch4_xdata[i-1];
	sp_end_y = ch4_ydata[i-1];
	sp_end_index = i-1;
	
	printf("sp_end_x: %f, sp_end_y: %f\n", sp_end_x, sp_end_y);
	PlotXY (SpikeTabPanelHandle, SPIKE_TAB_GRAPH, &sp_start_x, &sp_start_y, 1, VAL_FLOAT, VAL_FLOAT,
		VAL_SCATTER, VAL_BOLD_X, VAL_SOLID, 1, VAL_RED);
	
	PlotXY (SpikeTabPanelHandle, SPIKE_TAB_GRAPH, &sp_end_x, &sp_end_y, 1, VAL_FLOAT, VAL_FLOAT,
		VAL_SCATTER, VAL_BOLD_X, VAL_SOLID, 1, VAL_RED);
	
	SetCtrlVal (SpikeTabPanelHandle, SPIKE_TAB_PULSE_WIDTH, (sp_end_x - sp_start_x));
	
	// write pulse width
	sprintf (msg, "pulse width\t%f", (sp_end_x - sp_start_x));
	WriteLine (saveDataFile, msg, -1);
	
	// spike leakage
	max = ch4_ydata[0];
	maxIndex = 0;
		  
    for (i = 0; i < dataBytes_ch4/4; i++) {
        if (ch4_ydata[i] > max) {
            max = ch4_ydata[i];
			maxIndex = i;
        }
    }

    printf("max = %f\n", max);
	
	PlotXY (SpikeTabPanelHandle, SPIKE_TAB_GRAPH, &ch4_xdata[maxIndex], &max, 1, VAL_FLOAT, VAL_FLOAT,
		VAL_SCATTER, VAL_BOLD_X, VAL_SOLID, 1, VAL_RED);
	
	SetCtrlVal (SpikeTabPanelHandle, SPIKE_TAB_SPIKE, max);
	
	// write spike leakage
	sprintf (msg, "spike leakage\t%f", max);
	WriteLine (saveDataFile, msg, -1);
	
	// incident
	max = ch1_ydata[0];
	maxIndex = 0;
		  
    for (i = 0; i < dataBytes_ch1/4; i++) {
        if (ch1_ydata[i] > max) {
            max = ch1_ydata[i];
			maxIndex = i;
        }
    }
	
	float sum=0, avg;
	for(i=maxIndex; i<(dataBytes_ch1/4); i++) {
		sum += ch1_ydata[i];
	}
	avg = sum/(i-maxIndex);
	SetCtrlVal (SpikeTabPanelHandle, SPIKE_TAB_INCIDENT, avg);
	/*PlotXY (SpikeTabPanelHandle, SPIKE_TAB_GRAPH, &avg, &ch1_ydata[dataBytes_ch1/4-10], 1, VAL_FLOAT, VAL_FLOAT,
		VAL_SCATTER, VAL_BOLD_X, VAL_SOLID, 1, VAL_RED);*/
	
	// write incident
	sprintf (msg, "incident\t%f", avg);
	WriteLine (saveDataFile, msg, -1);
	
	// graph label
	SetCtrlAttribute (SpikeTabPanelHandle, SPIKE_TAB_GRAPH, ATTR_XNAME, "Time (us)");
	SetCtrlAttribute (SpikeTabPanelHandle, SPIKE_TAB_GRAPH, ATTR_YNAME, "Incident Power(dBm)");
	
	// y scale
	SetAxisScalingMode(SpikeTabPanelHandle, SPIKE_TAB_GRAPH, VAL_LEFT_YAXIS, VAL_MANUAL,
		-70,
		50);
	
	CloseFile(saveDataFile);
	
	// ch1 rename graph file
	sprintf(inciChar, "%f", avg);
	strcpy(oriFileName, pulseSetting);
	strcat(oriFileName,"spike_ch1.txt");
	strcpy(newFileName, pulseSetting);
	strcat(newFileName,"spike_ch1_");
	strcat(newFileName, inciChar);
	strcat(newFileName, ".txt");
	
	if (GetProjectDir (projectDir) < 0)
		FmtOut ("Project is untitled\n");
	else {
		MakePathname (projectDir, oriFileName, oriFullPath);
		MakePathname (projectDir, newFileName, newFullPath);
		printf("%s %s\n", oriFullPath, newFullPath);
	}
	RenameFile(oriFullPath, newFullPath);
	
	// ch4 rename graph file
	sprintf(inciChar, "%f", avg);
	strcpy(oriFileName, pulseSetting);
	strcat(oriFileName,"spike_ch4.txt");
	strcpy(newFileName, pulseSetting);
	strcat(newFileName,"spike_ch4_");
	strcat(newFileName, inciChar);
	strcat(newFileName, ".txt");
	
	MakePathname (projectDir, oriFileName, oriFullPath);
	MakePathname (projectDir, newFileName, newFullPath);
	RenameFile(oriFullPath, newFullPath);
	return 0;
}
