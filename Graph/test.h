/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  RE_PANEL                         1       /* callback function: Repanel */
#define  RE_PANEL_READ_BT                 2       /* control type: command, callback function: ReadCallback */
#define  RE_PANEL_GRAPH                   3       /* control type: graph, callback function: (none) */
#define  RE_PANEL_INCIDENT                4       /* control type: numeric, callback function: (none) */
#define  RE_PANEL_RECOVERY                5       /* control type: numeric, callback function: (none) */
#define  RE_PANEL_TEXTMSG                 6       /* control type: textMsg, callback function: (none) */
#define  RE_PANEL_TEXTMSG_2               7       /* control type: textMsg, callback function: (none) */

#define  SPIKEPANEL                       2       /* callback function: Sppanel */
#define  SPIKEPANEL_READ_BT               2       /* control type: command, callback function: ReadSPCallback */
#define  SPIKEPANEL_GRAPH                 3       /* control type: graph, callback function: (none) */
#define  SPIKEPANEL_INCIDENT              4       /* control type: numeric, callback function: (none) */
#define  SPIKEPANEL_SPIKE_ERG             5       /* control type: numeric, callback function: (none) */
#define  SPIKEPANEL_TEXTMSG_5             6       /* control type: textMsg, callback function: (none) */
#define  SPIKEPANEL_PULSE_WIDTH           7       /* control type: numeric, callback function: (none) */
#define  SPIKEPANEL_TEXTMSG_4             8       /* control type: textMsg, callback function: (none) */
#define  SPIKEPANEL_FLAT_LEAK             9       /* control type: numeric, callback function: (none) */
#define  SPIKEPANEL_TEXTMSG_3             10      /* control type: textMsg, callback function: (none) */
#define  SPIKEPANEL_SPIKE                 11      /* control type: numeric, callback function: (none) */
#define  SPIKEPANEL_TEXTMSG               12      /* control type: textMsg, callback function: (none) */
#define  SPIKEPANEL_TEXTMSG_2             13      /* control type: textMsg, callback function: (none) */


     /* Control Arrays: */

          /* (no control arrays in the resource file) */


     /* Menu Bars, Menus, and Menu Items: */

          /* (no menu bars in the resource file) */


     /* Callback Prototypes: */

int  CVICALLBACK ReadCallback(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ReadSPCallback(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK Repanel(int panel, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK Sppanel(int panel, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif
