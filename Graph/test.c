#include <visa.h>
#include <formatio.h>
#include <ansi_c.h>
#include <cvirte.h>		
#include <userint.h>
#include <string.h>
#include "test.h"

#define RE_CH1 501
#define RE_CH4 501

#define SP_CH1 501
#define SP_CH4 501

static int RepanelHandle;
static int SppanelHandle;

unsigned char StringToChar (unsigned char *string)
{
	int i;
	unsigned char tmp, tmp2;
	int length;
	length = strlen(string);
	
	if(length == 1) {
		string[1] = string[0];
		string[0] = '0';
	}
	tmp = string[0]-48;
	tmp2 = string[1]-48;
	if(tmp > 9) {
		tmp = string[0] - 97 + 10;
	}
	tmp = tmp*16;
	
	if(tmp2 > 9) {
		tmp2 = string[1] - 97 + 10;
	}
	return tmp+tmp2;
}

int main (int argc, char *argv[])
{
	if (InitCVIRTE (0, argv, 0) == 0)
		return -1;	/* out of memory */
	if ((RepanelHandle = LoadPanel (0, "test.uir", RE_PANEL)) < 0)
		return -1;
	if ((SppanelHandle = LoadPanel (0, "test.uir", SPIKEPANEL)) < 0)
		return -1;
	DisplayPanel (RepanelHandle);
	DisplayPanel (SppanelHandle);
	RunUserInterface ();
	RunUserInterface ();
	
	return 0;
}

int CVICALLBACK ReadCallback (int panel, int control, int event,
							  void *callbackData, int eventData1, int eventData2)
{
	FILE* logFile;
	FILE* saveFile;
	int error;
	int count = 0;
	float ch1_xdata[RE_CH1];
	float ch1_ydata[RE_CH1];
	float ch4_xdata[RE_CH4];
	float ch4_ydata[RE_CH4];
	unsigned char buf[10];
	unsigned char bufc[4];
	char *string = NULL;
	char msg[100];
	char *temp;
	int length;
	int i;
	float start;
	float re_start_x, re_end_x, re_end_y;
	switch (event)
	{
		case EVENT_COMMIT:
			char path[MAX_PATHNAME_LEN];
			if (FileSelectPopupEx ("", "", "", "Select ch1 graph data file",
								   VAL_OK_BUTTON, 0, 0, path) <= 0)
				return 0;
			
			i = 0;
			logFile = OpenFile (path, VAL_READ_ONLY, VAL_OPEN_AS_IS, VAL_ASCII);
			
			while ((error = ReadLine (logFile, buf, -1)) != -2) {
			    if (error == -1) {
			        error = GetFmtIOError ();
					MessagePopup("Error", GetFmtIOErrorString (error));
			    }
				bufc[3-count] = StringToChar(buf);
				
				count++;
				if (count == 4) {
					ch1_ydata[i] = *(float *) bufc;
					i++;
					count = 0;
				}
				
			}
			
			float start = 22.5;
			for (i=0; i<RE_CH1; i++){
				ch1_xdata[i] = start + i*0.01;
			}
			
			SetCtrlVal (panel, RE_PANEL_INCIDENT, ch1_ydata[10]);
			
			PlotXY (panel, RE_PANEL_GRAPH, &ch1_xdata[10], &ch1_ydata[10], 1, VAL_FLOAT, VAL_FLOAT,
				VAL_SCATTER, VAL_BOLD_X, VAL_SOLID, 1, VAL_RED);
			
			PlotXY (panel, RE_PANEL_GRAPH, ch1_xdata, ch1_ydata, RE_CH1, VAL_FLOAT, VAL_FLOAT,
				VAL_THIN_LINE, VAL_NO_POINT, VAL_SOLID, 1, VAL_GREEN);
			/*
			PlotXY (panel, PANEL_GRAPH, &ydata[501], &xdata[1000], 1, VAL_FLOAT, VAL_FLOAT,
				VAL_SCATTER, VAL_ASTERISK, VAL_SOLID, 1, VAL_RED);
			PlotXY (panel, PANEL_GRAPH, &ydata[1200], &xdata[1200], 1, VAL_FLOAT, VAL_FLOAT,
				VAL_SCATTER, VAL_ASTERISK, VAL_SOLID, 1, VAL_RED);
			PlotXY (panel, PANEL_GRAPH, &ydata[1300], &xdata[1300], 1, VAL_FLOAT, VAL_FLOAT,
				VAL_SCATTER, VAL_ASTERISK, VAL_SOLID, 1, VAL_RED);
			PlotXY (panel, PANEL_GRAPH, &ydata[1400], &xdata[1400], 1, VAL_FLOAT, VAL_FLOAT,
				VAL_SCATTER, VAL_ASTERISK, VAL_SOLID, 1, VAL_RED);*/
			
			CloseFile(logFile);
			
			// ch4
			if (FileSelectPopupEx ("", "", "", "Select ch4 graph data file",
								   VAL_OK_BUTTON, 0, 0, path) <= 0)
				return 0;
			i=0;
			logFile = OpenFile (path, VAL_READ_ONLY, VAL_OPEN_AS_IS, VAL_ASCII);
			
			while ((error = ReadLine (logFile, buf, -1)) != -2) {
			    if (error == -1) {
			        error = GetFmtIOError ();
					MessagePopup("Error", GetFmtIOErrorString (error));
			    }
				bufc[3-count] = StringToChar(buf);
				
				count++;
				if (count == 4) {
					ch4_ydata[i] = *(float *) bufc;
					i++;
					count = 0;
				}
			}
			
			start = 22.5;
			for (i=0; i<RE_CH4; i++){
				ch4_xdata[i] = start + i*0.01;
			}
			re_start_x = ch1_xdata[RE_CH4/4/2];
			for (i=0; i<RE_CH4; i++){
				if ((ch4_xdata[i] - re_start_x) < 0.01 && (ch4_xdata[i] - re_start_x) > -0.01) break;
			}
			re_start_x = ch4_xdata[i-1];
			printf("x2: %f. y2: %f\n", ch4_xdata[i-1], ch4_ydata[i-1]);
			
			PlotXY (panel, RE_PANEL_GRAPH, ch4_xdata, ch4_ydata, RE_CH4, VAL_FLOAT, VAL_FLOAT,
				VAL_THIN_LINE, VAL_NO_POINT, VAL_SOLID, 1, VAL_MAGENTA);
			
			PlotXY (panel, RE_PANEL_GRAPH, &ch4_xdata[i-1], &ch4_ydata[i-1], 1, VAL_FLOAT, VAL_FLOAT,
				VAL_SCATTER, VAL_BOLD_X, VAL_SOLID, 1, VAL_RED);
			
			CloseFile(logFile);
			
			float end = (ch4_ydata[RE_CH4-1]+ch4_ydata[RE_CH4-2]+ch4_ydata[RE_CH4-3])/3;
			printf("3 end average: %f\n", end);
			
			re_end_y = end-3;
			printf("end - 3dB : %f\n", re_end_y);
			
			for (i=0; i<RE_CH4; i++){
				if ((ch4_ydata[i]) > re_end_y) break;
			}
			
			re_end_x = ch4_xdata[i-1];
			
			PlotXY (panel, RE_PANEL_GRAPH, &ch4_xdata[i-1], &ch4_ydata[i-1], 1, VAL_FLOAT, VAL_FLOAT,
				VAL_SCATTER, VAL_BOLD_X, VAL_SOLID, 1, VAL_RED);
			
			printf("end: %f - start: %f = recovery: %f\n", re_end_x, re_start_x, re_end_x - re_start_x);
			SetCtrlVal (panel, RE_PANEL_RECOVERY, (re_end_x - re_start_x));
			
			// graph label
			SetCtrlAttribute (panel, RE_PANEL_GRAPH, ATTR_XNAME, "Time (us)");
			SetCtrlAttribute (panel, RE_PANEL_GRAPH, ATTR_YNAME, "Incident Power(dBm)");
			
			// y scale
			SetAxisScalingMode(panel, SPIKEPANEL_GRAPH, VAL_LEFT_YAXIS, VAL_MANUAL,
				-70,
				50);
			break;
	}
	return 0;
}


int CVICALLBACK Repanel (int panel, int event, void *callbackData,
						 int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_GOT_FOCUS:

			break;
		case EVENT_LOST_FOCUS:

			break;
		case EVENT_CLOSE:
			QuitUserInterface(0);
			DiscardPanel (RepanelHandle);
			break;
	}
	return 0;
}

int CVICALLBACK Sppanel (int panel, int event, void *callbackData,
						 int eventData1, int eventData2)
{
	switch (event)
	{
		case EVENT_GOT_FOCUS:

			break;
		case EVENT_LOST_FOCUS:

			break;
		case EVENT_CLOSE:
			QuitUserInterface(0);
			DiscardPanel (SppanelHandle);
			break;
	}
	return 0;
}

int CVICALLBACK ReadSPCallback (int panel, int control, int event,
								void *callbackData, int eventData1, int eventData2)
{
	FILE* logFile;
	FILE* saveFile;
	int error;
	int count = 0;
	float ch1_xdata[SP_CH1];
	float ch1_ydata[SP_CH1];
	float ch4_xdata[SP_CH4];
	float ch4_ydata[SP_CH4];
	unsigned char buf[10];
	unsigned char bufc[4];
	char *string = NULL;
	char msg[100];
	char *temp;
	int i;
	float ch2_start_x, ch4_end_y;
	float sp_start_x, sp_start_y, sp_end_x, sp_end_y, sp_start_index, sp_end_index;
	switch (event)
	{
		case EVENT_COMMIT:
			
			char path[MAX_PATHNAME_LEN];

			
			
			// ch1 graph
			
			if (FileSelectPopupEx ("", "", "", "Select ch1 graph data file",
								   VAL_OK_BUTTON, 0, 0, path) <= 0)
				return 0;
			
			i = 0;
			logFile = OpenFile (path, VAL_READ_ONLY, VAL_OPEN_AS_IS, VAL_ASCII);
			
			while ((error = ReadLine (logFile, buf, -1)) != -2) {
			    if (error == -1) {
			        error = GetFmtIOError ();
					MessagePopup("Error", GetFmtIOErrorString (error));
			    }
				bufc[3-count] = StringToChar(buf);
				
				count++;
				if (count == 4) {
					ch1_ydata[i] = *(float *) bufc;
					i++;
					count = 0;
				}
				
			}
			
			float start = -5;
			for (i=0; i<SP_CH1; i++){
				ch1_xdata[i] = start + i*0.01;
			}
			
			PlotXY (panel, SPIKEPANEL_GRAPH, ch1_xdata, ch1_ydata, SP_CH1, VAL_FLOAT, VAL_FLOAT,
				VAL_THIN_LINE, VAL_NO_POINT, VAL_SOLID, 1, VAL_GREEN);
		
			
			CloseFile(logFile);
			
			// ch4 graph
			if (FileSelectPopupEx ("", "", "", "Select ch4 graph data file",
								   VAL_OK_BUTTON, 0, 0, path) <= 0)
				return 0;
			
			i=0;
			logFile = OpenFile (path, VAL_READ_ONLY, VAL_OPEN_AS_IS, VAL_ASCII);
			
			while ((error = ReadLine (logFile, buf, -1)) != -2) {
			    if (error == -1) {
			        error = GetFmtIOError ();
					MessagePopup("Error", GetFmtIOErrorString (error));
			    }
				bufc[3-count] = StringToChar(buf);
				
				count++;
				if (count == 4) {
					ch4_ydata[i] = *(float *) bufc;
					i++;
					count = 0;
				}
			}
			
			
			start = -5;
			for (i=0; i<SP_CH4; i++){
				ch4_xdata[i] = start + i*0.01;
			}
			
			PlotXY (panel, SPIKEPANEL_GRAPH, ch4_xdata, ch4_ydata, SP_CH4, VAL_FLOAT, VAL_FLOAT,
				VAL_THIN_LINE, VAL_NO_POINT, VAL_SOLID, 1, VAL_MAGENTA);
			
			CloseFile(logFile);
			
			// end 3 points average = flat leakage
			ch4_end_y = (ch4_ydata[SP_CH4-1]+ch4_ydata[SP_CH4-2]+ch4_ydata[SP_CH4-3])/3;
			printf("3 end average: %f\n", ch4_end_y);
			SetCtrlVal (panel, SPIKEPANEL_FLAT_LEAK, ch4_end_y);
			
			// spike start, end, pulse width
			for (i=0; i<SP_CH4; i++){
				if ((ch4_ydata[i]) > ch4_end_y) break;
			}
			
			sp_start_x = ch4_xdata[i];
			sp_start_y = ch4_ydata[i];
			sp_start_index = i;
			
			for (; i<SP_CH4; i++){
				if ((ch4_ydata[i]) <= ch4_end_y) break;
			}
			
			sp_end_x = ch4_xdata[i-1];
			sp_end_y = ch4_ydata[i-1];
			sp_end_index = i-1;
			
			printf("sp_end_x: %f, sp_end_y: %f\n", sp_end_x, sp_end_y);
			PlotXY (panel, SPIKEPANEL_GRAPH, &sp_start_x, &sp_start_y, 1, VAL_FLOAT, VAL_FLOAT,
				VAL_SCATTER, VAL_BOLD_X, VAL_SOLID, 1, VAL_RED);
			
			PlotXY (panel, SPIKEPANEL_GRAPH, &sp_end_x, &sp_end_y, 1, VAL_FLOAT, VAL_FLOAT,
				VAL_SCATTER, VAL_BOLD_X, VAL_SOLID, 1, VAL_RED);
			
			SetCtrlVal (panel, SPIKEPANEL_PULSE_WIDTH, (sp_end_x - sp_start_x));
			
			// spike leakage
			float max = ch4_ydata[0];
			int maxIndex = 0;

		    for (i = 0; i < SP_CH4; i++) {
		        if (ch4_ydata[i] > max) {
		            max = ch4_ydata[i];
					maxIndex = i;
		        }
		    }

		    printf("max = %f\n", max);
			
			PlotXY (panel, SPIKEPANEL_GRAPH, &ch4_xdata[maxIndex], &max, 1, VAL_FLOAT, VAL_FLOAT,
				VAL_SCATTER, VAL_BOLD_X, VAL_SOLID, 1, VAL_RED);
			
			SetCtrlVal (panel, SPIKEPANEL_SPIKE, max);

			// spike leakage erg
			float spikeErg;
			float tmpAdd=0;
			for (i=sp_start_index; i< sp_end_index; i++) {
				tmpAdd += pow(10.0, (ch4_ydata[i]/10));
			}
			printf("add : %f W\n", tmpAdd*100);
			spikeErg = tmpAdd*100*0.01;
			printf("spike erg : %f W\n", spikeErg);
			
			
			SetCtrlVal (panel, SPIKEPANEL_SPIKE_ERG, spikeErg);
			
			// incident
			SetCtrlVal (panel, SPIKEPANEL_INCIDENT, ch1_ydata[SP_CH1-10]);
			PlotXY (panel, SPIKEPANEL_GRAPH, &ch1_xdata[SP_CH1-10], &ch1_ydata[SP_CH1-10], 1, VAL_FLOAT, VAL_FLOAT,
				VAL_SCATTER, VAL_BOLD_X, VAL_SOLID, 1, VAL_RED);
			
			// graph label
			SetCtrlAttribute (panel, SPIKEPANEL_GRAPH, ATTR_XNAME, "Time (us)");
			SetCtrlAttribute (panel, SPIKEPANEL_GRAPH, ATTR_YNAME, "Incident Power(dBm)");
			
			// y scale
			SetAxisScalingMode(panel, SPIKEPANEL_GRAPH, VAL_LEFT_YAXIS, VAL_MANUAL,
				-70,
				50);
			break;
	}
	return 0;
}
